# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

LOCAL_PATH := $(call my-dir)
MY_LOCAL_PATH := $(LOCAL_PATH)/../..

#--------------------------------------Utility-------------------------------------------------
include $(CLEAR_VARS)

LOCAL_PATH := $(MY_LOCAL_PATH)/iTransfer

LOCAL_C_INCLUDES += $(LOCAL_PATH) \
		

LOCAL_CFLAGS += -DANDROID -D_LINUX -DDEBUGLOG -D_INDENT_DB_PRINT  -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS -D_POSIX_PER_PROCESS_TIMER_SOURCE -D_PTHREADS -DHAVE_CONFIG_H -DUSE_JNI -D_PRINT_LOG -fexceptions

LOCAL_MODULE    := iTransfer
LOCAL_SRC_FILES := ifaddrs.c \
		Controller.cpp \
		ConvertUtility.cpp \
		DeviceInterface.cpp \
		iTransfer.cpp \
		iTransferCInterface.cpp \
		JNILinker.cpp \
		NotifyHandler.cpp \
		OwnAndFriendsInfo.cpp \
		ReceiveFile.cpp \
		SendFile.cpp \
		Socket.cpp \
		TcpSocketController.cpp \
		UdpMessages.cpp \
		UdpPacketCreatorAndParser.cpp

LOCAL_LDLIBS := -llog	 
include $(BUILD_SHARED_LIBRARY)
#--------------------------------------Utility-------------------------------------------------