#ifndef  ReceiveFile_h
#define  ReceiveFile_h

#include "Socket.h"
#include "CommonData.h"


#define  RECEIVE_BUFFER_SIZE   4096


class Socket;
class NotifyHandler;

class CReceiveFile
{
private:
	FILE  *m_fpWriteFile;
	bool m_bIsRunningFileReceivingThread;

	char m_sFileName[200];
	std::string m_sFileReceivePath;
	unsigned char m_sBuffer[RECEIVE_BUFFER_SIZE];
	
	int m_nNumOfFile;
	unsigned long long m_nFileDataSize;
	unsigned long long m_nRemainingDataSize;

	time_t m_tStartTime;
	time_t m_tEndTime;
	time_t m_tDiffTime;

	Socket  *m_cReceiver;
	NotifyHandler *m_cNotifyHandler;

protected:
	void OpenFile();

	void ReceiveFileInitInfo();

	int ReceiveFileInfoLenMsg();
	bool ReceiveFileInfo();
	bool ReceiveFileData();
	void ReceiveFileFinishSignal();
	void SendFileFinishResponseSignal();

	void CloseFile();


public:
	CReceiveFile(NotifyHandler *notifyHandler, std::string &fileReceivePath);

	void SetReceiver(Socket  *receiver);
	void RemoveReceiver();

	void ReceiveFile();
};


#endif