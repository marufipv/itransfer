#ifndef  _TCP_SOCKET_CONTROLLER_H
#define  _TCP_SOCKET_CONTROLLER_H

#include "CommonData.h"


class NotifyHandler;
class OwnAndFriendsInfo;
class Socket;
class CSendFile;
class CReceiveFile;
class Controller;

class TcpSocketsController
{
private:
	bool m_bIsClient;
	bool m_bIsProgramClosed;
	bool m_bIsRunningSendFileThread;
	bool m_bIsClosedFileReceivingThread;
	bool m_bIsCloseSessionClient;

	long long m_nOwnBindIp;
	int m_nOwnTcpListenPort;

	IPVLongType m_nCurrentReceiver;
	EntityInfo m_stCurRecverInfo;

	NotifyHandler *m_cNotifyHandler;
	OwnAndFriendsInfo *m_cOwnAndFriendsInfo;
	
	Socket *m_cTcpListenSock;
	Socket *m_cTcpFileSendRecvSocket;

	CSendFile *m_cSendFile;
	CReceiveFile  *m_cReceiveFile;
	Controller *m_cController;

protected:
	void CreateTcpListenSocket();
	void AcceptNewSocket();
	void HandleTcpAcceptSocket();
	std::thread ThreadTcpAcceptSocket();

	void HandleRecvFile();
	std::thread ThreadRecvFile();
	void HandleSendFile(std::vector<std::string> filePathList);
	std::thread ThreadSendFile(std::vector<std::string> filePathList);

	void SetFileSenderReceiverObject(Socket *obj);

	bool ConnectWithFriend(IPVLongType friendId);


public:
	TcpSocketsController(Controller *controller, NotifyHandler *notifyHandler, OwnAndFriendsInfo *ownAndFriendInfo, std::string &fileReceivePath);
	~TcpSocketsController();

	bool SendFile(IPVLongType friendId, std::vector<std::string> filePathList);
	void CloseCurrentSession(bool callFromDestructor = false);
};



#endif