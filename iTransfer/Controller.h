#ifndef _CONTROLLER_H
#define _CONTROLLER_H


#include "CommonData.h"
#include "iTransfer.h"


struct DataBuffer;

class NotifyHandler;
class DeviceInterface;
class UdpMessages;
class Socket;
class OwnAndFriendsInfo;
class TcpSocketsController;

class Controller
{
private:
	bool m_bIsRunningUDPDataReceivingThread;
	bool m_bIsClosedUDPDataReceivingThread;

	IPVLongType m_nUserName;
	std::string m_sFileReceivingPath;

	DataBuffer m_stDataBuffer;

	unsigned int m_nOwnBindIp;
	int m_nOwnTcpListenPort;

	NotifyHandler *m_cNotifyHandler;
	DeviceInterface *m_cDeviceInterface;

	UdpMessages *m_cUdpMessage;
	OwnAndFriendsInfo *m_cOwnAndFriendsInfo;

	TcpSocketsController *m_cTcpSocketsController;
	Socket *m_cUdpMainSock;


protected:
	void GatherHostInterface();
	void CreateUDPGeneralSocket();

	void HandleUDPDataReceiveThread();
	std::thread StartUDPDataReceiveThread();
	void StopUDPDataReceiveThread();


public:
	Controller(IPVLongType userName, std::string fileReceivePath);
	~Controller();

	void FindNearbyFriend();
	bool SendChatMessage(IPVLongType friendId, std::string message);

	void SendFile(IPVLongType friendId, std::vector<std::string> filePathList);
	void CloseCurrentSession();

	void SetNotifyFriendPresents(void(*ptr)(IPVLongType));
	void SetNotifyChatMessages(void(*ptr)(IPVLongType, std::string));
	void SetNotifyFileTransferInfo(void(*ptr)(int, std::string, int));
};


#endif