#ifdef USE_JNI
#include "iTransferCInterface.h"

#include <jni.h>

void NotifyiTransferFriendPresentsJNI(IPVLongType friendID);
void NotifyChatMessageJNI(IPVLongType friendID, std::string chatMessage);
void NotifyFileTransferInfoJNI(int type, std::string fileName, int receivedRatio);

extern "C" {
JNIEXPORT void JNICALL Java_com_iTransfer_init(JNIEnv *env, jobject obj, jlong userName, jstring fileReceivePath);

JNIEXPORT void JNICALL Java_com_iTransfer_FindFriend(JNIEnv *env, jobject obj);
JNIEXPORT jboolean JNICALL Java_com_iTransfer_SendChatMessage(JNIEnv *env, jobject obj, jlong friendName, jstring msg);
JNIEXPORT void JNICALL Java_com_iTransfer_SendFile(JNIEnv *env, jobject obj, jlong FriendName, jobjectArray sendFileList);

JNIEXPORT void JNICALL Java_com_iTransfer_CloseCurrentSession(JNIEnv *env, jobject obj);
JNIEXPORT void JNICALL Java_com_iTransfer_CloseiTransfer(JNIEnv *env, jobject obj);
}
#endif