
#ifdef USE_JNI
#include "iTransferCInterface.h"
#include <android/log.h>

CiTransfer *iTransfer = NULL;

extern "C" void iTransfer_init(IPVLongType userName, std::string fileReceivePath)
{
	__android_log_print(ANDROID_LOG_VERBOSE, "iTransfer", "Inside init.");
	if (iTransfer == NULL)
	{
		iTransfer = new CiTransfer(userName, fileReceivePath);
		__android_log_print(ANDROID_LOG_VERBOSE, "iTransfer", "Constructor called");

	}
}

extern "C" void iTransfer_FindFriend()
{
	if (iTransfer != NULL){
		iTransfer->FindNearbyFriend();
	}
}


extern "C" bool iTransfer_SendChatMsg(IPVLongType friendId, std::string message)
{
	if (iTransfer != NULL){
		return iTransfer->SendChatMessage(friendId, message);
	}
}


extern "C" void iTransfer_SendFile(IPVLongType FriendName, std::vector<std::string> sendFileList)
{
	if (iTransfer != NULL)
	{
		iTransfer->SendFile(FriendName, sendFileList);
	}
}

extern "C" void iTransfer_CloseCurrentSession()
{
	if (iTransfer != NULL)
	{
		iTransfer->CloseCurrentSession();
	}
}

extern "C" 	void iTransfer_CloseiTransfer()
{
	if (iTransfer != NULL)
	{
		delete iTransfer;
		iTransfer = NULL;
	}
}


#endif