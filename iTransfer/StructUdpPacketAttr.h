#ifndef _UDP_PACKET_ATTR_H
#define _UDP_PACKET_ATTR_H

#include "CommonData.h"


struct EntityInfo;

struct PacketAttribute
{
	unsigned char m_sPacketId[PACKET_ID_SIZE];

	EntityInfo m_stOwnInfo;
	EntityInfo m_stFriendInfo;

	bool m_bIsExistRawData;

	void Reset()
	{
		m_stOwnInfo.Reset();
		m_stFriendInfo.Reset();
		m_bIsExistRawData = false;
	}

	void SetPacketAttribute(EntityInfo &userInfo, EntityInfo &friendInfo, bool usePrevPackId = false, bool isExistData = false)
	{
		m_stOwnInfo = userInfo;
		m_stFriendInfo = friendInfo;

		if (usePrevPackId == false){
			SetRandomId(m_sPacketId, PACKET_ID_SIZE);
		}

		m_bIsExistRawData = isExistData;
	}

protected:
	void SetRandomId(unsigned char *s, int len) {
		std::ostringstream os;
		os << m_stOwnInfo.m_nEntityId;
		std::string userId = os.str();

		len = len - userId.size();
		static const char alphanum[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		for (int i = 0; i < len; ++i) {
			s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
		}

		memcpy(&s[len], userId.c_str(), userId.size());
	}
};


#endif