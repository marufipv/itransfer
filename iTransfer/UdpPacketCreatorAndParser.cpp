
#include "UdpPacketCreatorAndParser.h"



int UdpPacketCreatorAndParser::CreatePacket(const enum UdpPacketType &packetType, const struct PacketAttribute  &packetAttribute, unsigned char *buffer, char *rawData, int rawDataLen)
{
	m_nPacketLen = 0;

	buffer[m_nPacketLen++] = char(packetType);

	buffer[m_nPacketLen++] = char(UPAT_PACKET_ID);
	buffer[m_nPacketLen++] = char(PACKET_ID_SIZE);
	memcpy(&buffer[m_nPacketLen], packetAttribute.m_sPacketId, PACKET_ID_SIZE);                             m_nPacketLen = m_nPacketLen + PACKET_ID_SIZE;

	if (packetAttribute.m_stOwnInfo.m_nEntityId != -1){
		buffer[m_nPacketLen++] = char(UPAT_CLIENT_ID);
		buffer[m_nPacketLen++] = char(8);
		Convert::LongToByteArray(packetAttribute.m_stOwnInfo.m_nEntityId, &buffer[m_nPacketLen]);           m_nPacketLen = m_nPacketLen + 8;
	}

	if (packetAttribute.m_stOwnInfo.m_nIp != -1){
		buffer[m_nPacketLen++] = char(UPAT_CLIENT_IP);
		buffer[m_nPacketLen++] = char(8);
		Convert::LongToByteArray(packetAttribute.m_stOwnInfo.m_nIp, &buffer[m_nPacketLen]);                 m_nPacketLen = m_nPacketLen + 8;
	}

	if (packetAttribute.m_stOwnInfo.m_nUdpPort != -1){
		buffer[m_nPacketLen++] = char(UPAT_CLIENT_UDP_PORT);
		buffer[m_nPacketLen++] = char(4);
		Convert::IntToByteArray(packetAttribute.m_stOwnInfo.m_nUdpPort, &buffer[m_nPacketLen]);             m_nPacketLen = m_nPacketLen + 4;
	}

	if (packetAttribute.m_stOwnInfo.m_nTcpListenPort != -1){
		buffer[m_nPacketLen++] = char(UPAT_CLIENT_TCP_LISTEN_PORT);
		buffer[m_nPacketLen++] = char(4);
		Convert::IntToByteArray(packetAttribute.m_stOwnInfo.m_nTcpListenPort, &buffer[m_nPacketLen]);       m_nPacketLen = m_nPacketLen + 4;
	}

	if (packetAttribute.m_stFriendInfo.m_nEntityId != -1){
		buffer[m_nPacketLen++] = char(UPAT_FRIEND_ID);
		buffer[m_nPacketLen++] = char(8);
		Convert::LongToByteArray(packetAttribute.m_stFriendInfo.m_nEntityId, &buffer[m_nPacketLen]);        m_nPacketLen = m_nPacketLen + 8;
	}

	if (packetAttribute.m_stFriendInfo.m_nIp != -1){
		buffer[m_nPacketLen++] = char(UPAT_FRIEND_IP);
		buffer[m_nPacketLen++] = char(8);
		Convert::LongToByteArray(packetAttribute.m_stFriendInfo.m_nIp, &buffer[m_nPacketLen]);              m_nPacketLen = m_nPacketLen + 8;
	}

	if (packetAttribute.m_stFriendInfo.m_nUdpPort != -1){
		buffer[m_nPacketLen++] = char(UPAT_FRIEND_UDP_PORT);
		buffer[m_nPacketLen++] = char(4);
		Convert::IntToByteArray(packetAttribute.m_stFriendInfo.m_nUdpPort, &buffer[m_nPacketLen]);          m_nPacketLen = m_nPacketLen + 4;
	}

	if (packetAttribute.m_stFriendInfo.m_nTcpListenPort != -1){
		buffer[m_nPacketLen++] = char(UPAT_FRIEND_TCP_LISTEN_PORT);
		buffer[m_nPacketLen++] = char(4);
		Convert::IntToByteArray(packetAttribute.m_stFriendInfo.m_nTcpListenPort, &buffer[m_nPacketLen]);    m_nPacketLen = m_nPacketLen + 4;
	}

	if (packetAttribute.m_bIsExistRawData){
		buffer[m_nPacketLen++] = char(UPAT_RAW_DATA);
		Convert::ShortToByteArray(rawDataLen, &buffer[m_nPacketLen]);                 m_nPacketLen = m_nPacketLen + 2;
		memcpy(&buffer[m_nPacketLen], rawData, rawDataLen);                           m_nPacketLen = m_nPacketLen + rawDataLen;
	}

	return m_nPacketLen;
}



void UdpPacketCreatorAndParser::ParsePacket(unsigned char *buffer, int packetLen, enum UdpPacketType &packetType, struct PacketAttribute &packetAttribute, struct DataBuffer &dataBuffer)
{
	m_nPacketLen = 0;
	packetAttribute.Reset();

	packetType = UdpPacketType(buffer[m_nPacketLen++]);

	while (m_nPacketLen < packetLen){
		UdpPacketAttributeType attrType = UdpPacketAttributeType(buffer[m_nPacketLen++]);

		switch (attrType)
		{
		case UPAT_PACKET_ID:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			memcpy(packetAttribute.m_sPacketId, &buffer[m_nPacketLen], m_nAttrLen);                          m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		case UPAT_CLIENT_ID:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			packetAttribute.m_stOwnInfo.m_nEntityId = Convert::ByteArrayToLong(&buffer[m_nPacketLen]);       m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		case UPAT_CLIENT_IP:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			packetAttribute.m_stOwnInfo.m_nIp = Convert::ByteArrayToLong(&buffer[m_nPacketLen]);             m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		case UPAT_CLIENT_UDP_PORT:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			packetAttribute.m_stOwnInfo.m_nUdpPort = Convert::ByteArrayToInt(&buffer[m_nPacketLen]);          m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		case UPAT_CLIENT_TCP_LISTEN_PORT:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			packetAttribute.m_stOwnInfo.m_nTcpListenPort = Convert::ByteArrayToInt(&buffer[m_nPacketLen]);     m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		case UPAT_FRIEND_ID:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			packetAttribute.m_stFriendInfo.m_nEntityId = Convert::ByteArrayToLong(&buffer[m_nPacketLen]);      m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		case UPAT_FRIEND_IP:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			packetAttribute.m_stFriendInfo.m_nIp = Convert::ByteArrayToLong(&buffer[m_nPacketLen]);            m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		case UPAT_FRIEND_UDP_PORT:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			packetAttribute.m_stFriendInfo.m_nUdpPort = Convert::ByteArrayToInt(&buffer[m_nPacketLen]);        m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		case UPAT_FRIEND_TCP_LISTEN_PORT:
			m_nAttrLen = int(buffer[m_nPacketLen++]);
			packetAttribute.m_stFriendInfo.m_nTcpListenPort = Convert::ByteArrayToInt(&buffer[m_nPacketLen]);  m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;
		
		case UPAT_RAW_DATA:
			packetAttribute.m_bIsExistRawData = true;
			m_nAttrLen = Convert::ByteArrayToShort(&buffer[m_nPacketLen]);                                     m_nPacketLen = m_nPacketLen + 2;
			dataBuffer.dataLen = m_nAttrLen;
			memcpy(dataBuffer.data, &buffer[m_nPacketLen], m_nAttrLen);                                        m_nPacketLen = m_nPacketLen + m_nAttrLen;
			break;

		default:
#ifdef _PRINT_LOG
			printLog("Unknown Attribute in Message.");
#endif		
			break;
		}
	}
}

