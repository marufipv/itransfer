#ifndef _CONVERT_UTILITY_H
#define _CONVERT_UTILITY_H


#include "CommonData.h"


namespace Convert
{
	std::string to_string(unsigned int value);
	std::string LongToString(long long value);
	std::string getInterfaceName(int type);

	void ShortToByteArray(unsigned short num, unsigned char *sByteArr);
	unsigned short ByteArrayToShort(unsigned char *sByteArr);

	void IntToByteArray(int num, unsigned char *sByteArr);
	int ByteArrayToInt(unsigned char *sByteArr);

	void LongToByteArray(unsigned long long num, unsigned char *sByteArr);
	unsigned long long ByteArrayToLong(unsigned char *sByteArr);

	void SOSleep(int Timeout);
    void CharIPAddressToInterger32(char *addr, unsigned int &iaddr);
    std::string Interger32IPAddresstoString(unsigned int  &addr);
}


#endif