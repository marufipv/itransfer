#ifndef _DEVICE_INTERFACE_H
#define _DEVICE_INTERFACE_H


#include "StructInterfaceInfo.h"
#include "CommonData.h"

#ifdef _WIN32
#include <winsock2.h>
#if !(defined(WINDOWS_UNIVERSAL) || defined(WINDOWS_PHONE_8))
#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")
#endif
#else
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <unistd.h>
#ifdef __ANDROID__
#include "ifaddrs.h"
#include <linux/if.h>
#include <linux/wireless.h>
#else
#ifdef __linux__
#include <linux/if_link.h>
#include <linux/if.h>
#include <ifaddrs.h>
#include <linux/wireless.h>
#else
#include <ifaddrs.h>
#endif
#endif
#endif



#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))


struct InterfaceInfo;

class DeviceInterface
{
private:


protected:
	std::string CalculateBroadcastAddress(std::string ipAddr, std::string mask);
	std::string GetBroadcastFromIpAddr(std::string ipAddr);

	void PushAnInterfaceAddressIntoList(unsigned int address, std::string Address, int interfaceType, unsigned int iBroadCastAdd, std::string sBroadCastAdd);
    int DetectInterfaceTypeForIOS(struct sockaddr_in(*interfaceAddr));

public:
	std::vector<InterfaceInfo> m_vInterfaceIP4AddressList;
	
	DeviceInterface();
	void GatherHostInterfaceInfo();
	void PrintInterfaceAddresses();
    
};




#endif