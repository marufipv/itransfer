#ifdef ITRANSFER_EXPORTS
#define ITRANSFER_API __declspec(dllexport) 
#else
#define ITRANSFER_API __declspec(dllimport) 
#endif


#ifndef _I_TRANSFER_H
#define _I_TRANSFER_H

#include<stdio.h>
#include<string>
#include<vector>

#ifdef WIN32
typedef __int64 IPVLongType;
#else 
typedef long long IPVLongType;
#endif


class Controller;

class CiTransfer{

public:
	CiTransfer(IPVLongType userName, std::string fileReceivePath);
	~CiTransfer();

	void FindNearbyFriend();
	bool SendChatMessage(IPVLongType friendId, std::string message);	
	void SendFile(IPVLongType friendId, std::vector<std::string> filePathList);

	void CloseCurrentSession();
	
	void NotifyFriendPresents(void(*ptr)(IPVLongType));
	void NotifyChatMessages(void(*ptr)(IPVLongType, std::string));
	void NotifyFileTransferInfo(void(*ptr)(int type, std::string fileName, int value));

private:
	Controller *m_cController;
};


#endif