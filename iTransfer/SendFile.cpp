
#include "SendFile.h"
#include <iostream>


CSendFile::CSendFile(NotifyHandler *notifyHandler) : 
m_cNotifyHandler(notifyHandler), 
m_cSender(NULL), 
m_fpReadFile(NULL),
m_bStopSendingFile(false)
{
}


void CSendFile::SetSender(Socket *sender)
{
#ifdef _PRINT_LOG
	printLog("CSendFile::SetSender()");
#endif

	m_cSender = sender;
}


void CSendFile::RemoveSender()
{
#ifdef _PRINT_LOG
	printLog("CSendFile::RemoveSender()");
#endif

	m_cSender = NULL;
}


void CSendFile::OpenFile(char *filePath)
{
#ifdef _PRINT_LOG
	printLog("SendFile::OpenFile()");
#endif

	m_fpReadFile = fopen(filePath, "rb");

	if (m_fpReadFile != NULL){
		fseek(m_fpReadFile, 0, SEEK_END);
		m_nFileSize = ftell(m_fpReadFile);

#ifdef _PRINT_LOG
		printLog("TotalFileSize: " + Convert::LongToString(m_nFileSize));
#endif
	}
}


void CSendFile::SendFileInfoSizeSignal(int lenOfFileInfo)
{
#ifdef _PRINT_LOG
	printLog("CSendFile::SendFileInfoSizeSignal()");
#endif

	int pos;
	unsigned char msg[5];

	msg[0] = char(TPT_FILE_INFO_SIZE);                       pos = 1;
	Convert::IntToByteArray(lenOfFileInfo, &msg[pos]);              pos = pos + 4;

	if (!m_bStopSendingFile){
		m_cSender->Send((char *)msg, pos);
	}
}


void CSendFile::SendFileInfo(char *filePath)
{
#ifdef _PRINT_LOG
	printLog("CSendFile::SendFileInfo()");
#endif

	int fileNameLen, msgLen;
	char fileName[200];


#ifdef USE_JNI
    //#ifndef _WIN32
    char *srcPos = (strrchr(filePath, '/') + 1);
#elif __APPLE__
    char *srcPos = (strrchr(filePath, '/') + 1);
#else
    char *srcPos = (strrchr(filePath, '\\') + 1);
#endif

	
	strcpy(fileName, srcPos);
	
	m_sFileName = std::string(fileName);
	fileNameLen = strlen(fileName);

	msgLen = 0;
	m_sBuffer[msgLen] = char(TPT_FILE_SEND_START);                   msgLen += 1;
	Convert::LongToByteArray(m_nFileSize, &m_sBuffer[msgLen]);              msgLen += 8;
	Convert::IntToByteArray(fileNameLen, &m_sBuffer[msgLen]);               msgLen += 4;
	strcpy((char*)&m_sBuffer[msgLen], fileName);                            msgLen += fileNameLen;

	SendFileInfoSizeSignal(msgLen);

	if (!m_bStopSendingFile){
		m_cSender->Send((char *)m_sBuffer, msgLen);

		m_cNotifyHandler->NotifyFileTransferInfo(NOTIFY_TYPE_RATIO, m_sFileName, 0);
		m_tStartTime = time(NULL);
	}
}


void CSendFile::SendFileData()
{
#ifdef _PRINT_LOG
	printLog("SendFile::SendFileData()");
#endif

	int readSize, sentRatio, lastRatio;
	unsigned long long remainingFileSize;

	remainingFileSize = m_nFileSize;
	fseek(m_fpReadFile, 0, SEEK_SET);

	lastRatio = 0;
	while (remainingFileSize > 0){
		if (m_bStopSendingFile){
			return;
		}

		if (remainingFileSize > SEND_BUFFER_SIZE){
			readSize = SEND_BUFFER_SIZE;
			remainingFileSize = remainingFileSize - SEND_BUFFER_SIZE;
		}
		else{
			readSize = remainingFileSize;
			remainingFileSize = 0;
//			std::cout << "LastPacketSize: " << readSize << std::endl;
		}

		fread(m_sBuffer, 1, readSize, m_fpReadFile);
		m_cSender->Send((char *)m_sBuffer, readSize);

		sentRatio = (((m_nFileSize - remainingFileSize) * 100) / m_nFileSize);
		if (((lastRatio + 5) <= sentRatio) && (sentRatio != 100)){
			m_cNotifyHandler->NotifyFileTransferInfo(NOTIFY_TYPE_RATIO, m_sFileName, sentRatio);
			lastRatio = sentRatio;
		}
	}
}


void CSendFile::SendFileFinishSignal()
{
#ifdef _PRINT_LOG
	printLog("SendFile::SendFileFinishSignal()");
#endif
	
	int msgLen = 0;
	int rate;

	if (m_bStopSendingFile){
		return;
	}

	m_sBuffer[msgLen] = char(TPT_FILE_SEND_FINISH);                  msgLen += 1;
	Convert::IntToByteArray(m_nFileSize, &m_sBuffer[msgLen]);               msgLen += 4;

	m_cSender->Send((char*)m_sBuffer, msgLen);

	m_cNotifyHandler->NotifyFileTransferInfo(NOTIFY_TYPE_RATIO, m_sFileName, 100);

	m_tEndTime = time(NULL);
	m_tDiffTime = (m_tEndTime - m_tStartTime);

#ifdef _PRINT_LOG
	printLog("======== Total Elapsed Time: " + Convert::to_string(m_tDiffTime));
	printLog("======== Total File Size: " + Convert::LongToString(m_nFileSize));
#endif

	if (m_tDiffTime){
		rate = ((m_nFileSize / m_tDiffTime) / (128 * 1024));
#ifdef _PRINT_LOG
		printLog("======== Data Rate: " + Convert::to_string(rate) + " (Mb/Second)");
#endif
	}
	else{
		rate = (m_nFileSize / (128 * 1024));
		if (rate){
#ifdef _PRINT_LOG
			printLog("======== Data Rate: " + Convert::to_string(rate) + " (Mb/Second)");
#endif
		}
		else{
			rate = 10;
#ifdef _PRINT_LOG
			printLog("======== Data Rate: " + Convert::to_string(rate) + " (Mb/Second)");
#endif
		}
	}
	m_cNotifyHandler->NotifyFileTransferInfo(NOTIFY_TYPE_RATE, m_sFileName, rate);
}


void CSendFile::CloseFile()
{
#ifdef _PRINT_LOG
	printLog("CSendFile::CloseFile()");
#endif

	if (m_fpReadFile != NULL){
		fclose(m_fpReadFile);
		m_fpReadFile = NULL;
	}
}


bool CSendFile::ReceiveFileFinishResponseSignal()
{
#ifdef _PRINT_LOG
	printLog("CSendFile::ReceiveFileFinishResponseSignal()");
#endif

	m_cSender->Recv((char *)m_sBuffer, SEND_BUFFER_SIZE);

	if (int(m_sBuffer[0]) == TPT_FILE_SEND_FINISH_RESPONSE){
		return true;
	}
	return false;
}


bool CSendFile::SendTheFile(char *filePath)
{
#ifdef _PRINT_LOG
	printLog("CSendFile::SendTheFile()");
#endif

	OpenFile(filePath);

	if (m_fpReadFile == NULL || m_cSender == NULL){
		return false;
	}

	SendFileInfo(filePath);
	SendFileData();
	SendFileFinishSignal();
//	ReceiveFileFinishResponseSignal();

	CloseFile();

	return true;
}


void CSendFile::SendNumOfFileInfo(int numOfFile)
{
#ifdef _PRINT_LOG
	printLog("CSendFile::SendNumOfFileInfo()");
#endif

	int pos;

	m_sBuffer[0] = char(TPT_FILE_SEND_INIT);                   pos = 1;
	Convert::IntToByteArray(numOfFile, &m_sBuffer[pos]);              pos = pos + 4;

	m_cSender->Send((char *)m_sBuffer, pos);
}


bool CSendFile::SendFile(std::vector<std::string> sendFilePath)
{
#ifdef _PRINT_LOG
	printLog("CSendFile::SendFile()");
#endif

	int numOfFile;

	m_bStopSendingFile = false;
	numOfFile = sendFilePath.size();
//	SendNumOfFileInfo(numOfFile);

#ifdef _PRINT_LOG
	printLog("NumOfFile: " + Convert::to_string(numOfFile));
#endif
	for (int i = 0; i < numOfFile; i++){
		if (!m_bStopSendingFile){
			SendTheFile((char *)sendFilePath[i].c_str());
		}
	}

	return true;
}


void CSendFile::StopSendingFile()
{
#ifdef _PRINT_LOG
	printLog("CSendFile::StopSendingFile()");
#endif

	m_bStopSendingFile = true;
}