
#include "NotifyHandler.h"
#ifdef __APPLE__
#include "ObjectivecInterface.h"
#endif


NotifyHandler::NotifyHandler() 
{
	NotifyFriendPresentsCallBack = NULL;
	NotifyChatMessagesCallBack = NULL;
	NotifyFileTransferInfoCallBack = NULL;
}


void NotifyHandler::SetNotifyFriendPresents(void(*ptr)(IPVLongType))
{
	NotifyFriendPresentsCallBack = ptr;
}


void NotifyHandler::NotifyFriendPresents(IPVLongType friendId)
{
#ifdef USE_JNI
		NotifyiTransferFriendPresentsJNI(friendId);
#elif __APPLE__
		NotifyiTransferFriendPresentsIos(friendId);
#else
	if (NotifyFriendPresentsCallBack != NULL){
		NotifyFriendPresentsCallBack(friendId);
	}
#endif
}


void NotifyHandler::SetNotifyChatMessages(void(*ptr)(IPVLongType, std::string))
{
	NotifyChatMessagesCallBack = ptr;
}


void NotifyHandler::NotifyChatMessages(IPVLongType friendId, std::string &message)
{
#ifdef USE_JNI
		NotifyChatMessageJNI(friendId, message);
#else
	if (NotifyChatMessagesCallBack != NULL){
		NotifyChatMessagesCallBack(friendId, message);
	}
#endif	
}


void NotifyHandler::SetNotifyFileTransferInfo(void(*ptr)(int, std::string, int))
{
	NotifyFileTransferInfoCallBack = ptr;
}

void NotifyHandler::NotifyFileTransferInfo(int type, std::string fileName, int value)
{
#ifdef USE_JNI
		NotifyFileTransferInfoJNI(type, fileName, value);
#elif __APPLE__
		NotifyFileTransferRateIos(type, fileName, value);
#else
	if (NotifyFileTransferInfoCallBack != NULL){
		NotifyFileTransferInfoCallBack(type, fileName, value);
	}
#endif
}