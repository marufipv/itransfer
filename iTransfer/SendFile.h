#ifndef SendFile_h
#define SendFile_h


#include <vector>
#include <ctime>
#include "CommonData.h"
#include "Socket.h"


#define SEND_BUFFER_SIZE  4096



class Socket;
class NotifyHandler;

class CSendFile
{
private:
	bool m_bStopSendingFile;
	FILE *m_fpReadFile;
	
	std::string m_sFileName;
	unsigned char m_sBuffer[SEND_BUFFER_SIZE];
	unsigned long long m_nFileSize;
	
	time_t m_tStartTime;
	time_t m_tEndTime;
	time_t m_tDiffTime;

	Socket *m_cSender;
	NotifyHandler *m_cNotifyHandler;


protected:
	void OpenFile(char *filePath);

	void SendFileInfoSizeSignal(int lenOfFileInfo);
	void SendFileInfo(char *filePath);
	void SendFileData();
	void SendFileFinishSignal();
	bool ReceiveFileFinishResponseSignal();

	void CloseFile();

	bool SendTheFile(char *filePath);
	void SendNumOfFileInfo(int numOfFile);


public:
	CSendFile(NotifyHandler  *notifyHandler);

	void SetSender(Socket *sender);
	void RemoveSender();

	bool SendFile(std::vector<std::string> sendFilePath);
	void StopSendingFile();
};


#endif