
#include "DeviceInterface.h"


#ifdef __APPLE__
#include <net/if.h>
#include <SystemConfiguration/SystemConfiguration.h>
#endif

#ifdef USE_JNI
#include <android/log.h>
#endif


DeviceInterface::DeviceInterface()
{

}

bool compareFunction(InterfaceInfo lhs, InterfaceInfo rhs)
{
	return lhs.m_nInterfaceType < rhs.m_nInterfaceType;
}


std::string DeviceInterface::CalculateBroadcastAddress(std::string ipAddr, std::string mask)
{
	int i;
	unsigned int ipArr[4], maskArr[4], broadCastArr[4];

	sscanf(ipAddr.c_str(), "%d.%d.%d.%d", &ipArr[0], &ipArr[1], &ipArr[2], &ipArr[3]);
	sscanf(mask.c_str(), "%d.%d.%d.%d", &maskArr[0], &maskArr[1], &maskArr[2], &maskArr[3]);

	for (i = 0; i < 4; i++){
		broadCastArr[i] = ipArr[i] | (maskArr[i] ^ 255);
	}

	return Convert::to_string(broadCastArr[0]) + "." + Convert::to_string(broadCastArr[1]) + "." + Convert::to_string(broadCastArr[2]) + "." + Convert::to_string(broadCastArr[3]);
}


std::string DeviceInterface::GetBroadcastFromIpAddr(std::string ipAddr)
{
	unsigned int ipArr[4];

	sscanf(ipAddr.c_str(), "%d.%d.%d.%d", &ipArr[0], &ipArr[1], &ipArr[2], &ipArr[3]);
	ipArr[3] = 255;

	return Convert::to_string(ipArr[0]) + "." + Convert::to_string(ipArr[1]) + "." + Convert::to_string(ipArr[2]) + "." + Convert::to_string(ipArr[3]);
}


void DeviceInterface::GatherHostInterfaceInfo()
{
#ifdef _PRINT_LOG
	printLog("DeviceInterface::GatherHostInterfaceInfo()");
#endif

	m_vInterfaceIP4AddressList.clear();

#if (defined(WINDOWS_UNIVERSAL) || defined(WINDOWS_PHONE_8))

	WSADATA wsaData;
	int iResult;

	DWORD dwError;
	int i = 0;

	struct hostent *remoteHost;

	struct in_addr addr;

	char **pAlias;

	int namelen = 1000;
	char *host_name = new char[namelen];

	int iRet = gethostname(host_name, namelen);

	remoteHost = gethostbyname(host_name);


	if (remoteHost == NULL) {
		dwError = WSAGetLastError();
		if (dwError != 0)
		{
			if (dwError == WSAHOST_NOT_FOUND)
			{
				return;
			}
			else if (dwError == WSANO_DATA)
			{
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		switch (remoteHost->h_addrtype) {
		case AF_INET:
			break;
		case AF_NETBIOS:
			break;
		case IFF_LOOPBACK:
			break;
		default:
			break;
		}

		i = 0;
		if (remoteHost->h_addrtype == AF_INET)
		{
			int interfacetype = INTERFACE_TYPE_WIFI;

			while (remoteHost->h_addr_list[i] != 0)
			{
				addr.s_addr = *(u_long *)remoteHost->h_addr_list[i];
				i++;

				u_int32_t uaddress = addr.s_addr;
				std::string address = Convert::Interger32IPAddresstoString(uaddress);

				std::string sBroadcastAddr = GetBroadcastFromIpAddr(address);
				u_int32_t  nBroadcastAddr = inet_addr(sBroadcastAddr.c_str());

				PushAnInterfaceAddressIntoList(uaddress, address, interfacetype, nBroadcastAddr, sBroadcastAddr);
			}
		}
	}

#elif _WIN32

	std::string sIpAddr, sMask, sBroadcastAddr;
	unsigned int  nIpAddr, nBroadcastAddr;
	int interfaceType;

	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = NULL;
	DWORD dwRetVal = 0;

	ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);
	pAdapterInfo = (IP_ADAPTER_INFO *)MALLOC(sizeof(IP_ADAPTER_INFO));
	if (pAdapterInfo == NULL) {
		printf("Error allocating memory needed to call GetAdaptersinfo\n");
		return;
	}

	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
		FREE(pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO *)MALLOC(ulOutBufLen);
		if (pAdapterInfo == NULL) {
			printf("Error allocating memory needed to call GetAdaptersinfo\n");
			return;
		}
	}

	if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR) {
		pAdapter = pAdapterInfo;
		while (pAdapter) {
			switch (pAdapter->Type) {
			case MIB_IF_TYPE_ETHERNET:
				interfaceType = INTERFACE_TYPE_ETHERNET;
				break;
			case IF_TYPE_IEEE80211:
				interfaceType = INTERFACE_TYPE_WIFI;
				break;
			case MIB_IF_TYPE_LOOPBACK:
				pAdapter = pAdapter->Next;
				continue;

				/*
				case MIB_IF_TYPE_OTHER:
				printf("Other\n");
				break;
				case MIB_IF_TYPE_TOKENRING:
				printf("Token Ring\n");
				break;
				case MIB_IF_TYPE_FDDI:
				printf("FDDI\n");
				break;
				case MIB_IF_TYPE_PPP:
				printf("PPP\n");
				break;
				case MIB_IF_TYPE_SLIP:
				printf("Slip\n");
				break;
				*/
			default:
				interfaceType = INTERFACE_TYPE_OTHER;
				break;
			}

			sIpAddr = pAdapter->IpAddressList.IpAddress.String;
			sMask = pAdapter->IpAddressList.IpMask.String;
			sBroadcastAddr = CalculateBroadcastAddress(sIpAddr, sMask);

			nIpAddr = inet_addr(sIpAddr.c_str());
			nBroadcastAddr = inet_addr(sBroadcastAddr.c_str());

			std::cout << "IP Address: " << sIpAddr << ":" << nIpAddr << " Mask: " << sMask << " Broadcast Address: " << sBroadcastAddr << std::endl;

			if (nIpAddr != 0){
				PushAnInterfaceAddressIntoList(nIpAddr, sIpAddr, interfaceType, nBroadcastAddr, sBroadcastAddr);
			}

			pAdapter = pAdapter->Next;
		}
	}
	else {
		printf("GetAdaptersInfo failed with error: %d\n", dwRetVal);
	}

	if (pAdapterInfo)
		FREE(pAdapterInfo);


#elif __linux__
	char sAddr[16];
	unsigned int nIpAddr, nBroadcastAddr;
	std::string sIpAddr, sBroadcastAddr, sNameOfInterface;

	struct ifconf ifc;
	struct ifreq ifr[10];
	int sd, ifc_num, i, type;

	sd = socket(PF_INET, SOCK_DGRAM, 0);

	if (sd > 0){
		ifc.ifc_len = sizeof(ifr);
		ifc.ifc_ifcu.ifcu_buf = (caddr_t)ifr;

		if (ioctl(sd, SIOCGIFCONF, &ifc) == 0)
		{
			ifc_num = ifc.ifc_len / sizeof(struct ifreq);
			std::cout << "NumOfFoundInterfaces: " << ifc_num << std::endl;

			for (i = 0; i < ifc_num; ++i)
			{
				if (ifr[i].ifr_addr.sa_family != AF_INET) {
					continue;
				}

				sNameOfInterface = std::string(ifr[i].ifr_name);

				if (sNameOfInterface.find("wlan") != std::string::npos)	{
					type = INTERFACE_TYPE_WIFI;
				}
				else if (sNameOfInterface.find("eth") != std::string::npos)	{
					type = INTERFACE_TYPE_ETHERNET;
				}
				else if (sNameOfInterface.find("lo") != std::string::npos)	{
					continue;
				}
				else {
					type = INTERFACE_TYPE_OTHER;
				}

				if (ioctl(sd, SIOCGIFADDR, &ifr[i]) == 0) {
					nIpAddr = ((struct sockaddr_in *)(&ifr[i].ifr_addr))->sin_addr.s_addr;
					inet_ntop(AF_INET, &nIpAddr, sAddr, 16);
					sIpAddr = std::string(sAddr);
				}
				if (ioctl(sd, SIOCGIFBRDADDR, &ifr[i]) == 0) {
					nBroadcastAddr = ((struct sockaddr_in *)(&ifr[i].ifr_broadaddr))->sin_addr.s_addr;
					inet_ntop(AF_INET, &nBroadcastAddr, sAddr, 16);
					sBroadcastAddr = std::string(sAddr);
				}

				std::cout << "IP Address: " << sIpAddr << " Broadcast Address: " << sBroadcastAddr << " Interface: " << sNameOfInterface << std::endl;
#ifdef USE_JNI
				__android_log_print(ANDROID_LOG_VERBOSE, "iTransfer", "IpAddress: %s, BroadCastAddr: %s, InterfaceType: %s", sIpAddr.c_str(), sBroadcastAddr.c_str(), sNameOfInterface.c_str());
#endif

				if (nIpAddr != 0){
					PushAnInterfaceAddressIntoList(nIpAddr, sIpAddr, type, nBroadcastAddr, sBroadcastAddr);
				}
			}
		}

		close(sd);
	}


#elif __APPLE__

	std::string adds;
	std::string sBroadcast;
	struct ifaddrs *interfaces = NULL;
	struct ifaddrs *temp_addr = NULL;
	int success = 0;

	success = getifaddrs(&interfaces);

	if (success == 0)
	{
		temp_addr = interfaces;

		while (temp_addr != NULL)
		{
			if (temp_addr->ifa_addr->sa_family == AF_INET && !(temp_addr->ifa_flags & IFF_LOOPBACK))
			{
				const char *c_style = inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr);
				adds = c_style;

				u_int32_t address;
				u_int32_t broadcastAddress;
				char *addsChar = (char *)adds.c_str();
				Convert::CharIPAddressToInterger32(addsChar, address);
				struct sockaddr_in*  tempInterfaceAddr = (struct sockaddr_in*)temp_addr->ifa_addr;
				const char *broadcast = inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_dstaddr)->sin_addr);
				sBroadcast = broadcast;
				char *braodcasrChar = (char *)sBroadcast.c_str();
				Convert::CharIPAddressToInterger32(braodcasrChar, broadcastAddress);
				PushAnInterfaceAddressIntoList(address, adds, DetectInterfaceTypeForIOS(tempInterfaceAddr), broadcastAddress, sBroadcast);

				std::string b_cast = Convert::Interger32IPAddresstoString(broadcastAddress);
				printf("the broadcast interface address %s\n", b_cast.c_str());
			}

			temp_addr = temp_addr->ifa_next;
		}
	}

	freeifaddrs(interfaces);

#endif

	sort(m_vInterfaceIP4AddressList.begin(), m_vInterfaceIP4AddressList.end(), compareFunction);
}


int DeviceInterface::DetectInterfaceTypeForIOS(struct sockaddr_in(*interfaceAddr))
{
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
    struct sockaddr_in tempInterfaceAddr;
    bzero(&tempInterfaceAddr, sizeof(tempInterfaceAddr));
    tempInterfaceAddr.sin_len = sizeof(tempInterfaceAddr);
    tempInterfaceAddr.sin_family = AF_INET;
    tempInterfaceAddr.sin_addr = interfaceAddr->sin_addr;
    
    
    SCNetworkReachabilityFlags flags;
    
    int interfaceType = INTERFACE_TYPE_OTHER;
    
    bool reachable = true;
    
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&tempInterfaceAddr);
    
    
    
    if (SCNetworkReachabilityGetFlags(reachability, &flags))
    {
        
        if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
        {
            reachable = false;
            
            if ((flags & kSCNetworkReachabilityFlagsReachable) && (flags & kSCNetworkReachabilityFlagsIsDirect))
            {
                interfaceType = INTERFACE_TYPE_WIFI;
            }
        }
        
        if (true == reachable && (flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
        {
            interfaceType = INTERFACE_TYPE_WIFI;
            
        }
        
        
        if (true == reachable && (((flags & kSCNetworkReachabilityFlagsConnectionOnDemand) != 0) ||
                                  (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
        {
            if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
            {
                interfaceType = INTERFACE_TYPE_WIFI;
            }
        }
        
        if (true == reachable && (flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
        {
            interfaceType = INTERFACE_TYPE_3G;
        }
    }
    
    return interfaceType;
#endif
    return INTERFACE_TYPE_OTHER;
}

void DeviceInterface::PushAnInterfaceAddressIntoList(unsigned int address, std::string Address, int interfaceType, unsigned int iBroadCastAdd, std::string sBroadCastAdd)
{
	if (interfaceType > 2){
		return;
	}

	InterfaceInfo newCIPVInterfaceInfo(address, Address, interfaceType, iBroadCastAdd, sBroadCastAdd);
	m_vInterfaceIP4AddressList.push_back(newCIPVInterfaceInfo);
}



void DeviceInterface::PrintInterfaceAddresses()
{
#ifdef _PRINT_LOG
	printLog("CIPVInterface::PrintInterfaceAddress()");
#endif

	int sizeOfInterfaceAddress = (int)m_vInterfaceIP4AddressList.size();

	for (int index = 0; index < sizeOfInterfaceAddress; index++){
		InterfaceInfo interfaceAddress = m_vInterfaceIP4AddressList.at(index);

#ifdef _PRINT_LOG
		printLog("Interface address: " + interfaceAddress.m_sHostAddr + "\tInterface Type: " + Convert::getInterfaceName(interfaceAddress.m_nInterfaceType) + "\tBroadcastAddr: " + interfaceAddress.m_sBroadcastAddr);
#endif
	}
}