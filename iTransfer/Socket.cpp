
#include "Socket.h"


Socket::Socket(bool isUdp) :
m_fd((int)INVALID_SOCKET),
m_nSocketType(-1),
m_nProtocolType(-1),
m_bIsRunningRecvingSocket(true)
{
	m_nSocketType = (isUdp ? SOCK_DGRAM : SOCK_STREAM);
	m_nProtocolType = (isUdp ? IPPROTO_UDP : IPPROTO_TCP);
}


bool Socket::CreateSocket()
{
#ifdef _WIN32
	WSAData data;
	WSAStartup(MAKEWORD(2, 2), &data);
#endif

	m_fd = (int)socket(AF_INET, m_nSocketType, m_nProtocolType);

	if (m_fd == INVALID_SOCKET || m_fd == -1){
		return false;
	}

	return true;
}


void Socket::SetSocket(int sock)
{
	m_fd = sock;
}


bool Socket::EnableSocketBroadcastOption()
{
#ifdef USE_JNI
	static int so_broadcast = 1;
	if (setsockopt(m_fd, SOL_SOCKET, SO_BROADCAST, &so_broadcast, sizeof so_broadcast) != 0){
		return false;
	}
#endif

#ifdef __APPLE__
	int noSigpipe = 1;
	if (setsockopt(m_fd, SOL_SOCKET, SO_BROADCAST, (void *)&noSigpipe, sizeof(noSigpipe)) < 0)	{
		return false;
	}
#endif

	return true;
}


bool Socket::SetRecvTimeOut()
{
	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;

	if (setsockopt(m_fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval))) {
		perror("setsockopt");
		return false;
	}

	return true;
}


bool Socket::BindSocket(unsigned long address, unsigned int port)
{
	memset((char*)&m_sSocketAddress, 0, sizeof(m_sSocketAddress));
	m_sSocketAddress.sin_family = AF_INET;
	m_sSocketAddress.sin_addr.s_addr = address;
	m_sSocketAddress.sin_port = htons(port);

	if (::bind(m_fd, (struct sockaddr *) &m_sSocketAddress, sizeof(m_sSocketAddress)) != 0) {
		return false;
	}

	return true;
}


bool Socket::Connect(unsigned int remoteAddress, unsigned int remotePort)
{
	struct sockaddr_in remoteaddr;
	memset((char *)&remoteaddr, 0, sizeof(remoteaddr));

	remoteaddr.sin_family = AF_INET;
	remoteaddr.sin_addr.s_addr = remoteAddress;
	remoteaddr.sin_port = htons(remotePort);

	if (::connect(m_fd, (struct sockaddr*)&remoteaddr, sizeof(struct sockaddr_in)) == 0) {
		return true;
	}

	return false;
}


int Socket::GetBindPort()
{
	return  ntohs(m_sSocketAddress.sin_port);
}


bool Socket::Listen()
{
	if (listen(m_fd, 3) == 0){
		return true;
	}
	else{
		return false;
	}
}


int Socket::AcceptConnection()
{
	return ::accept(m_fd, NULL, NULL);
}


int Socket::Send(const char *sendData, int dataSize)
{
	return  ::send(m_fd, sendData, dataSize, 0);
}


int Socket::Recv(char *recvBuf, int bufSize)
{
	while (m_bIsRunningRecvingSocket){
		m_nDataSize = ::recv(m_fd, recvBuf, bufSize, 0);

		if (m_nDataSize == -1){
			continue;
		}
		else if (m_nDataSize == 0){
#ifdef _PRINT_LOG
			printLog("Connection Closed from Peer.");
#endif		
			return -2;
		}
		return m_nDataSize;
	}

#ifdef _PRINT_LOG
	printLog("Connection Closed by Client.");
#endif		
	return -2;
}


void Socket::StopReceivingData()
{
#ifdef _PRINT_LOG
	printLog("CSocket::StopReceivingData()");
#endif		

	m_bIsRunningRecvingSocket = false;
}



int Socket::SendTo(unsigned char *sendData, int dataSize, unsigned int sendAddr, unsigned int sendPort)
{
	memset(&m_sPeerAddr, 0, sizeof(m_sPeerAddr));
	m_sPeerAddr.sin_family = AF_INET;
	m_sPeerAddr.sin_addr.s_addr = sendAddr;
	m_sPeerAddr.sin_port = htons(sendPort);

	return ::sendto(m_fd, (const char*)sendData, dataSize, 0, (struct sockaddr *) &m_sPeerAddr, sizeof(m_sPeerAddr));
}


int Socket::RecvFrom(unsigned char *recvBuf, int bufSize, unsigned int &recvAddr, unsigned int &recvPort)
{
	memset(&m_sPeerAddr, 0, sizeof(m_sPeerAddr));
	m_nAddrSize = sizeof(m_sPeerAddr);
	
	m_nDataSize = ::recvfrom(m_fd, (char*)recvBuf, bufSize, 0,	(sockaddr*)&m_sPeerAddr, (socklen_t *)&m_nAddrSize);

	recvAddr = m_sPeerAddr.sin_addr.s_addr;
	recvPort = ntohs(m_sPeerAddr.sin_port);

	return m_nDataSize;
}


int Socket::CloseSocket(int fd)
{
	int rc;
	if (m_fd == INVALID_SOCKET){
		return -1;
	}

	if (fd == -1){
		fd = m_fd;
	}

#ifdef WIN32
	rc = closesocket(fd);
#else
	rc = close(fd);
#endif

	printLog("Close Socket RC: " + Convert::to_string(rc));
	return rc;
}


Socket::~Socket()
{
	CloseSocket();
}
