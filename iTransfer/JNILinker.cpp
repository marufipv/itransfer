#ifdef USE_JNI
#include "JNILinker.h"
#include <vector>
#include <android/log.h>

static JavaVM *jvm = NULL;
static jobject callback = NULL;

extern "C" JNIEXPORT void JNICALL Java_com_iTransfer_init(JNIEnv *env, jobject obj, jlong userName, jstring fileReceivePath)
{
	env->GetJavaVM(&jvm);
	callback = env->NewGlobalRef(obj);

	const char *cFilePath = env->GetStringUTFChars(fileReceivePath, JNI_FALSE);

	iTransfer_init(userName, cFilePath);
	__android_log_print(ANDROID_LOG_VERBOSE, "iTransfer", "Init called from jni linker");
	//env->ReleaseStringUTFChars(fileReceivePath, 0);
}

extern "C" JNIEXPORT void JNICALL Java_com_iTransfer_FindFriend(JNIEnv *env, jobject obj)
{
	iTransfer_FindFriend();
}


JNIEXPORT jboolean JNICALL Java_com_iTransfer_SendChatMessage(JNIEnv *env, jobject obj, jlong friendName, jstring msg)
{
	const char *chatMsg = env->GetStringUTFChars(msg, JNI_FALSE);

	return iTransfer_SendChatMsg(friendName, chatMsg);
}


extern "C" JNIEXPORT void JNICALL Java_com_iTransfer_SendFile(JNIEnv *env, jobject obj, jlong FriendName, jobjectArray sendFileList)
{
	std::vector<std::string> fileVector;
	std::string tmp;
	int stringCount = env->GetArrayLength(sendFileList);

	for (int i = 0; i<stringCount; i++) {
		jstring string = (jstring)env->GetObjectArrayElement(sendFileList, i);
		const char *rawString = env->GetStringUTFChars(string, 0);
		tmp = std::string(rawString);

		fileVector.push_back(tmp);

		//env->ReleaseStringUTFChars(string, 0);
	}
	
	iTransfer_SendFile(FriendName, fileVector);
}


extern "C" JNIEXPORT void JNICALL Java_com_iTransfer_CloseCurrentSession(JNIEnv *env, jobject obj)
{
	iTransfer_CloseCurrentSession();
}


extern "C"  JNIEXPORT void JNICALL Java_com_iTransfer_CloseiTransfer(JNIEnv *env, jobject obj)
{
	iTransfer_CloseiTransfer();
}


void NotifyiTransferFriendPresentsJNI(IPVLongType friendID)
{
	
	if (jvm == NULL || callback == NULL)
		return;

	__android_log_print(ANDROID_LOG_VERBOSE, "iTransfer", "NotifyiTransferFriendPresentsJNI event fired.");

	JNIEnv *env = NULL;
	jint res = 0;

#ifndef ANDROID
	res = jvm->AttachCurrentThread((void **)&env, NULL);
#else
	res = (jvm)->AttachCurrentThread(&env, NULL);
#endif

	if (res >= 0)
	{
		jclass cls = env->GetObjectClass(callback);

		jmethodID mid = env->GetMethodID(cls, "NotifyiTransferFriendPresents", "(J)V");

		env->CallVoidMethod(callback, mid, friendID);

		//__android_log_print(ANDROID_LOG_VERBOSE, "iTransfer", "NotifyiTransferFriendPresentsJNI just fired the event.");

		jvm->DetachCurrentThread();
	}
}


void NotifyChatMessageJNI(IPVLongType friendID, std::string chatMessage)
{
	if (jvm == NULL || callback == NULL)
		return;

	JNIEnv *env = NULL;
	jint res = 0;

#ifndef ANDROID
	res = jvm->AttachCurrentThread((void **)&env, NULL);
#else
	res = (jvm)->AttachCurrentThread(&env, NULL);
#endif

	if (res >= 0)
	{
		jclass cls = env->GetObjectClass(callback);

		jmethodID mid = env->GetMethodID(cls, "NotifyChatMessage", "(JLjava/lang/String;)V");

		const char* chatM = chatMessage.c_str();

		jstring message = env->NewStringUTF(chatM);

		env->CallVoidMethod(callback, mid, friendID, message);

		jvm->DetachCurrentThread();
	}
}


void NotifyFileTransferInfoJNI(int type, std::string fileName, int receivedRatio)
{
//	__android_log_print(ANDROID_LOG_VERBOSE, "iTransfer", "--&&&&&----NotifyFileTransferInfoJNI event fired.");

	if (jvm == NULL || callback == NULL)
		return;


	JNIEnv *env = NULL;
	jint res;

#ifndef ANDROID
	res = jvm->AttachCurrentThread((void **)&env, NULL);
#else
	res = (jvm)->AttachCurrentThread(&env, NULL);
#endif
	if (res >= 0)
	{
		jclass cls = env->GetObjectClass(callback);

		jmethodID mid = env->GetMethodID(cls, "NotifyFileTransferInfo", "(ILjava/lang/String;I)V");

		const char* tmp = fileName.c_str();

		jstring filename = env->NewStringUTF(tmp);

		env->CallVoidMethod(callback, mid, type, filename, receivedRatio);

		jvm->DetachCurrentThread();
	}
}



#endif