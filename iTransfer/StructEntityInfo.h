#ifndef _STRUCT_ENTITY_INFO_H
#define _STRUCT_ENTITY_INFO_H

#include "CommonData.h"



struct EntityInfo
{
	IPVLongType m_nEntityId;
	long long m_nIp;
	int m_nUdpPort;
	int m_nTcpListenPort;

	EntityInfo()
	{
		m_nEntityId = -1;
		m_nIp = -1;
		m_nUdpPort = -1;
		m_nTcpListenPort = -1;
	}

	void Reset()
	{
		m_nEntityId = -1;
		m_nIp = -1;
		m_nUdpPort = -1;
		m_nTcpListenPort = -1;
	}

};

#endif