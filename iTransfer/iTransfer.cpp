
#include "iTransfer.h"
#include "Controller.h"


CiTransfer::CiTransfer(IPVLongType userName, std::string fileReceivePath) : m_cController(NULL)
{
	if (m_cController == NULL){
		m_cController = new Controller(userName, fileReceivePath);
	}
}


CiTransfer::~CiTransfer()
{
	if (m_cController != NULL){
		delete m_cController;
		m_cController = NULL;
	}
}


void CiTransfer::FindNearbyFriend()
{
	if (m_cController != NULL){
		m_cController->FindNearbyFriend();
	}
}


bool CiTransfer::SendChatMessage(IPVLongType friendId, std::string message)
{
	if (m_cController != NULL){
		return m_cController->SendChatMessage(friendId, message);
	}
	return false;
}


void CiTransfer::SendFile(IPVLongType FriendName, std::vector<std::string> filePathList)
{
	if (m_cController != NULL){
		m_cController->SendFile(FriendName, filePathList);
	}
}

void CiTransfer::CloseCurrentSession()
{
	if (m_cController != NULL){
		m_cController->CloseCurrentSession();
	}
}


void CiTransfer::NotifyFriendPresents(void(*ptr)(IPVLongType))
{
	if (m_cController != NULL){
		m_cController->SetNotifyFriendPresents(ptr);
	}
}


void CiTransfer::NotifyChatMessages(void(*ptr)(IPVLongType, std::string))
{
	if (m_cController != NULL){
		m_cController->SetNotifyChatMessages(ptr);
	}
}


void CiTransfer::NotifyFileTransferInfo(void(*ptr)(int, std::string, int))
{
	if (m_cController != NULL){
		m_cController->SetNotifyFileTransferInfo(ptr);
	}
}