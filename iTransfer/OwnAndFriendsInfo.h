#ifndef _OWN_AND_FRIENDS_INFO_H
#define _OWN_AND_FRIENDS_INFO_H

#include "CommonData.h"


struct EntityInfo;

class OwnAndFriendsInfo
{
private:
	EntityInfo m_stOwnInfo;
	EntityInfo m_stFriendInfo;

	std::map<IPVLongType, EntityInfo> m_mFriendList;
	std::map<IPVLongType, EntityInfo>::iterator m_itFriendList;


public:
	OwnAndFriendsInfo(IPVLongType userId);
	void SetOwnIp(long long ip);
	void SetOwnUdpPort(int udpPort);
	void SetOwnTcpListenPort(int tcpListenPort);

	EntityInfo GetOwnInfo();
	IPVLongType GetOwnId();
	long long GetOwnIp();
	int GetOwnUdpPort();
	int GetOwnTcpListenPort();

	void ClearFriendList();
	void UpdateFriendsList(EntityInfo &friendInfo);
	
	bool IsFriendExist(IPVLongType friendId);
	EntityInfo GetFriendInfo(IPVLongType friendId);
	long long GetFriendIp(IPVLongType friendId);
	int GetFriendUdpPort(IPVLongType friendId);
	int GetFriendTcpListenPort(IPVLongType friendId);
};


#endif // !_FRIENDS_INFO_H
