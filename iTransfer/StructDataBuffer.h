#ifndef _DATA_BUFFER_H
#define _DATA_BUFFER_H


#include "CommonData.h"


struct DataBuffer
{
	unsigned char data[MAXIMUM_BUFFER_SIZE];
	int dataLen;
	unsigned int clientIp;
	unsigned int clientPort;

	DataBuffer(){
		memset(data, '\0', MAXIMUM_BUFFER_SIZE);
		dataLen = -1;
		clientIp = 0;
		clientPort = 0;
	}
};


#endif