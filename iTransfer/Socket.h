#ifndef  _SOCKET_H
#define  _SOCKET_H

#include "CommonData.h"



class Socket
{
private:
	int m_fd;
	int m_nSocketType;
	int m_nProtocolType;

	int m_nAddrSize;
	int m_nDataSize;

	struct sockaddr_in m_sSocketAddress;
	struct sockaddr_in m_sPeerAddr;
	
	bool m_bIsRunningRecvingSocket;


protected:



public:
	Socket(bool isUdp);
	~Socket();

	bool CreateSocket();
	void SetSocket(int sock);
	int CloseSocket(int fd = -1);

	bool EnableSocketBroadcastOption();
	bool SetRecvTimeOut();

	bool Connect(unsigned int remoteAddress, unsigned int remotePort);
	bool BindSocket(unsigned long address, unsigned int port);
	int  GetBindPort();
	bool Listen();
	int AcceptConnection();

	int Send(const char *sendData, int dataSize); 
	int Recv(char *recvBuf, int bufSize);
	void StopReceivingData();

	int SendTo(unsigned char *sendData, int dataSize, unsigned int sendAddr, unsigned int sendPort);
	int RecvFrom(unsigned char *recvBuf, int bufSize, unsigned int &recvAddr, unsigned int &recvPort);

};


#endif