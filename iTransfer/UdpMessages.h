#ifndef _UDP_MESSAGES_H
#define _UDP_MESSAGES_H


#include "StructUdpPacketAttr.h"
#include "CommonData.h"

enum  UdpPacketType;
struct DataBuffer;
struct EntityInfo;
struct PacketAttribute;
struct InterfaceInfo;

class NotifyHandler;
class DeviceInterface;
class UdpPacketCreatorAndParser;
class Socket;
class OwnAndFriendsInfo;
class Controller;



struct UdpPackRandId
{
	unsigned char m_sBroadcastRequestId[PACKET_ID_SIZE];
	unsigned char m_sBroadcastPeerResponseId[PACKET_ID_SIZE];
	unsigned char m_sBroadcastClientResponseId[PACKET_ID_SIZE];

	unsigned char m_sChatMessage[PACKET_ID_SIZE];
	unsigned char m_sChatMessageConfirm[PACKET_ID_SIZE];
};


class UdpMessages
{
private:
	bool m_bIsSender;
	bool m_bUsePrevPackId;
	bool m_bIsChatMessageSent;
	
	UdpPackRandId m_stPackId;
	UdpPacketType m_eUdpPacketType;

	DataBuffer m_stDataBuffer;
	PacketAttribute m_stCreatePacketAttr;
	PacketAttribute m_stParsedPacketAttr;
	InterfaceInfo *m_stInterfaceInfo;


	EntityInfo m_stOwnInfo;                                                     
	EntityInfo m_stCurFriendInfo;                                                    // Used In Chat Message.
	EntityInfo m_stFriendInfo;                                                       // Used in BroadCast. 

	int m_nPacketSize;
	int m_nSentMsgSize;

	NotifyHandler *m_cNotifyHandler;
	Controller *m_cController;
	Socket *m_cUdpMainSock;
	OwnAndFriendsInfo *m_cOwnAndFriendsInfo;
	DeviceInterface *m_cDeviceInterface;
	UdpPacketCreatorAndParser *m_cUdpPacketCreatorAndParser;


protected:
	std::string GetHotSpotAddr(std::string broadCastAddr);
	
	void HandleBroadcastRequestMessage();
	void HandleBroadcastPeerResponse();
	void HandleBroadcastClientResponse();

	void HandleChatMessage();
	void HandleChatConfirmMessage();


public:
	UdpMessages(DeviceInterface *deviceInterface, Socket *udpMainSock, OwnAndFriendsInfo *ownAndFriendsInfo, NotifyHandler *notifyHandler, Controller *mainController);
	~UdpMessages();

	void ProcessReceivedData(struct DataBuffer  &receivedDataBuffer);
	void SendBroadCastRequest();

	bool SendChatMessage(IPVLongType friendId, std::string message);

};


#endif