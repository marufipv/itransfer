
#include "TcpSocketController.h"



TcpSocketsController::TcpSocketsController(Controller *controller, NotifyHandler *notifyHandler, OwnAndFriendsInfo *ownAndFriendInfo, std::string &fileReceivePath) :
m_bIsClient(true),
m_bIsProgramClosed(false),
m_bIsRunningSendFileThread(false),
m_cTcpFileSendRecvSocket(NULL),
m_cTcpListenSock(NULL),
m_cSendFile(NULL),
m_cReceiveFile(NULL),
m_cController(controller),
m_cNotifyHandler(notifyHandler),
m_cOwnAndFriendsInfo(ownAndFriendInfo)
{
	m_cSendFile = new CSendFile(m_cNotifyHandler);
	m_cReceiveFile = new CReceiveFile(m_cNotifyHandler, fileReceivePath);

	CreateTcpListenSocket();

	std::thread tAcceptSocketThread(ThreadTcpAcceptSocket());
	tAcceptSocketThread.detach();
}


TcpSocketsController::~TcpSocketsController()
{
	m_bIsProgramClosed = true;
	CloseCurrentSession(true);

	if (m_cTcpListenSock != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cTcpListenSock.");
#endif
		m_cTcpListenSock->CloseSocket();

		delete m_cTcpListenSock;
		m_cTcpListenSock = NULL;
	}

	if (m_cSendFile != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cSendFile.");
#endif
		delete m_cSendFile;
		m_cSendFile = NULL;
	}

	if (m_cReceiveFile != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cReceiveFile.");
#endif
		delete m_cReceiveFile;
		m_cReceiveFile = NULL;
	}
}


void TcpSocketsController::CreateTcpListenSocket()
{
	if (m_cTcpListenSock != NULL){
		return;
	}

	m_nOwnBindIp = INADDR_ANY;

	m_cTcpListenSock = new Socket(false);
	m_cTcpListenSock->CreateSocket();

	while (true){
		m_nOwnTcpListenPort = rand() % 1000 + 55000;
		if (m_cTcpListenSock->BindSocket(m_nOwnBindIp, m_nOwnTcpListenPort)){
			break;
		}
	}

	m_cTcpListenSock->Listen();
	m_cOwnAndFriendsInfo->SetOwnTcpListenPort(m_nOwnTcpListenPort);

#ifdef _PRINT_LOG
	printLog("OwnTcpPort: " + Convert::to_string(m_nOwnTcpListenPort));
#endif
}


void TcpSocketsController::AcceptNewSocket()
{
//  
//	if (m_cTcpFileSendRecvSocket != NULL){
//		return;
//	}

	if ((m_cTcpListenSock != NULL) && (m_bIsProgramClosed == false)){
#ifdef _PRINT_LOG
		printLog("-----------------------------------Wait To Accept Socket.");
#endif

		int sock = m_cTcpListenSock->AcceptConnection();
#ifdef _PRINT_LOG
		printLog("Accept Sock: " + Convert::to_string(sock));
#endif
		if (m_bIsProgramClosed == true){
#ifdef _PRINT_LOG
			printLog("Return: Program already closed");
#endif
			return;
		}

		if (m_cTcpFileSendRecvSocket == NULL){
			m_cTcpFileSendRecvSocket = new Socket(false);
			m_cTcpFileSendRecvSocket->SetSocket(sock);
			m_cTcpFileSendRecvSocket->SetRecvTimeOut();
			SetFileSenderReceiverObject(m_cTcpFileSendRecvSocket);

			m_bIsClient = false;
#ifdef _PRINT_LOG
			printLog("Creating New Sock Obj.");
#endif
		}
		else{
#ifdef _PRINT_LOG
			printLog("Closing sock: " + Convert::to_string(sock));
#endif
			m_cTcpFileSendRecvSocket->CloseSocket(sock);
		}
	}
}


void TcpSocketsController::HandleTcpAcceptSocket()
{
	while (m_cTcpListenSock != NULL){
		AcceptNewSocket();
		Convert::SOSleep(100);
	}
}


std::thread TcpSocketsController::ThreadTcpAcceptSocket()
{
	return std::thread([=] { HandleTcpAcceptSocket(); });
}


void TcpSocketsController::HandleRecvFile()
{
#ifdef _PRINT_LOG
	printLog("StartedFileReceiving Thread.");
#endif

	m_bIsCloseSessionClient = false;
	m_bIsClosedFileReceivingThread = false;

	if (m_cReceiveFile != NULL){
		m_cReceiveFile->ReceiveFile();
	}

#ifdef _PRINT_LOG
	printLog("=============================================ClosedFileReceiving Thread.");
#endif
	m_bIsClosedFileReceivingThread = true;

	if (m_bIsCloseSessionClient == false){
		CloseCurrentSession();
		m_cNotifyHandler->NotifyFriendPresents(-1);
	}
}


std::thread TcpSocketsController::ThreadRecvFile()
{
	return std::thread([=] { HandleRecvFile(); });
}


void TcpSocketsController::SetFileSenderReceiverObject(Socket *obj)
{
#ifdef _PRINT_LOG
	printLog("TcpSocketsController::SetFileSenderReceiverObject()");
#endif

	if (m_cSendFile != NULL){
		m_cSendFile->SetSender(obj);
	}

	if (m_cReceiveFile != NULL){
		m_cReceiveFile->SetReceiver(obj);
	}

	std::thread recvFile = ThreadRecvFile();
	recvFile.detach();
}


bool TcpSocketsController::ConnectWithFriend(IPVLongType friendId)
{
#ifdef _PRINT_LOG
	printLog("TcpSocketsController::ConnectWithFriend()");
#endif

	if (m_cOwnAndFriendsInfo->IsFriendExist(friendId)){
		m_stCurRecverInfo = m_cOwnAndFriendsInfo->GetFriendInfo(friendId);
	}
	else {
		return false;
	}

	if (m_cTcpFileSendRecvSocket != NULL){
#ifdef _PRINT_LOG
		printLog("Close existing connection.");
#endif
		m_cTcpFileSendRecvSocket->CloseSocket();
		delete m_cTcpFileSendRecvSocket;
		m_cTcpFileSendRecvSocket = NULL;
	}

	m_cTcpFileSendRecvSocket = new Socket(false);
	if (m_cTcpFileSendRecvSocket->CreateSocket()){
		m_cTcpFileSendRecvSocket->SetRecvTimeOut();
		if (m_cTcpFileSendRecvSocket->Connect(m_stCurRecverInfo.m_nIp, m_stCurRecverInfo.m_nTcpListenPort)){
#ifdef _PRINT_LOG
			printLog("Connection is established with: " + Convert::LongToString(friendId));
#endif
			SetFileSenderReceiverObject(m_cTcpFileSendRecvSocket);

			m_bIsClient = true;
			m_nCurrentReceiver = friendId;
			return true;
		}
		else{
#ifdef _PRINT_LOG
			printLog("Connection couldn't be established.");
#endif
			m_cTcpFileSendRecvSocket->CloseSocket();
			delete m_cTcpFileSendRecvSocket;
			m_cTcpFileSendRecvSocket = NULL;
		}
	}

	return false;
}


bool TcpSocketsController::SendFile(IPVLongType friendId, std::vector<std::string> filePathList)
{
#ifdef _PRINT_LOG
	printLog("CiTransferAgent::SendFile()");
#endif
	if (m_bIsClient){
		if (m_nCurrentReceiver != friendId){
			if (ConnectWithFriend(friendId) == false){
				return false;
			}
		}
	}

	if (m_cSendFile == NULL){
#ifdef _PRINT_LOG
		printLog("CSendFile is NULL.");
#endif
		return false;
	}

	if (m_bIsRunningSendFileThread == false){
		m_bIsRunningSendFileThread = true;
		std::thread sendFile = ThreadSendFile(filePathList);
		sendFile.detach();
	}
	return true;
}


void TcpSocketsController::HandleSendFile(std::vector<std::string> filePathList)
{
	m_cSendFile->SendFile(filePathList);
	m_bIsRunningSendFileThread = false;
}


std::thread TcpSocketsController::ThreadSendFile(std::vector<std::string> filePathList)
{
	return std::thread([=] { HandleSendFile(filePathList); });
}


void TcpSocketsController::CloseCurrentSession(bool callFromDestructor)
{
#ifdef _PRINT_LOG
	printLog("TcpSocketsController::CloseCurrentSession()");
#endif
	
	m_bIsClient = true;
	m_bIsCloseSessionClient = true;
	m_nCurrentReceiver = -1;

	if (m_cTcpFileSendRecvSocket != NULL){
#ifdef _PRINT_LOG
		printLog("Close File Receiving Thread.");
#endif
		m_cTcpFileSendRecvSocket->StopReceivingData();
		while (m_bIsClosedFileReceivingThread != true){
			Convert::SOSleep(1);
		}

#ifdef _PRINT_LOG
		printLog("Close File Sending Thread.");
#endif
		m_cSendFile->StopSendingFile();
		while (m_bIsRunningSendFileThread == true){
			Convert::SOSleep(1);
		}

		m_cTcpFileSendRecvSocket->CloseSocket();

#ifdef _PRINT_LOG
		printLog("Delete m_cTcpFileSendRecvSocket.");
#endif
		delete m_cTcpFileSendRecvSocket;
		m_cTcpFileSendRecvSocket = NULL;
	}

	if (m_cSendFile != NULL){
		m_cSendFile->RemoveSender();
	}

	if (m_cReceiveFile != NULL){
		m_cReceiveFile->RemoveReceiver();
	}

	if (callFromDestructor == false){
		m_cOwnAndFriendsInfo->ClearFriendList();
		m_cController->FindNearbyFriend();
	}
}
