
#include "OwnAndFriendsInfo.h"


OwnAndFriendsInfo::OwnAndFriendsInfo(IPVLongType userId)
{
	m_stOwnInfo.m_nEntityId = userId;
}


void OwnAndFriendsInfo::SetOwnIp(long long ip)
{
	m_stOwnInfo.m_nIp = ip;
}


void OwnAndFriendsInfo::SetOwnUdpPort(int udpPort)
{
	m_stOwnInfo.m_nUdpPort = udpPort;
}


void OwnAndFriendsInfo::SetOwnTcpListenPort(int tcpListenPort)
{
	m_stOwnInfo.m_nTcpListenPort = tcpListenPort;
}


EntityInfo OwnAndFriendsInfo::GetOwnInfo()
{
	return m_stOwnInfo;
}


IPVLongType OwnAndFriendsInfo::GetOwnId()
{
	return m_stOwnInfo.m_nEntityId;
}


long long OwnAndFriendsInfo::GetOwnIp()
{
	return m_stOwnInfo.m_nIp;
}


int OwnAndFriendsInfo::GetOwnUdpPort()
{
	return m_stOwnInfo.m_nUdpPort;
}


int OwnAndFriendsInfo::GetOwnTcpListenPort()
{
	return m_stOwnInfo.m_nTcpListenPort;
}


void OwnAndFriendsInfo::ClearFriendList()
{
	m_mFriendList.clear();
}


void OwnAndFriendsInfo::UpdateFriendsList(EntityInfo &friendInfo)
{
	m_mFriendList.insert(std::make_pair(friendInfo.m_nEntityId, friendInfo));
}


bool OwnAndFriendsInfo::IsFriendExist(IPVLongType friendId)
{
	m_itFriendList = m_mFriendList.find(friendId);

	if (m_itFriendList != m_mFriendList.end()){
		return true;
	}
	else{
		return false;
	}
}


EntityInfo OwnAndFriendsInfo::GetFriendInfo(IPVLongType friendId)
{
	if (IsFriendExist(friendId)){
		return m_itFriendList->second;
	}
	else{
		m_stFriendInfo.Reset();
		return m_stFriendInfo;
	}
}


long long OwnAndFriendsInfo::GetFriendIp(IPVLongType friendId)
{
	if (IsFriendExist(friendId)){
		return m_itFriendList->second.m_nIp;
	}
	else{
		return -1;
	}
}


int OwnAndFriendsInfo::GetFriendUdpPort(IPVLongType friendId)
{
	if (IsFriendExist(friendId)){
		return m_itFriendList->second.m_nUdpPort;
	}
	else{
		return -1;
	}
}


int OwnAndFriendsInfo::GetFriendTcpListenPort(IPVLongType friendId)
{
	if (IsFriendExist(friendId)){
		return m_itFriendList->second.m_nTcpListenPort;
	}
	else{
		return -1;
	}
}

