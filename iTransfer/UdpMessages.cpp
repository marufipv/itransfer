
#include "UdpMessages.h"



UdpMessages::UdpMessages(DeviceInterface *deviceInterface, Socket *udpMainSock, OwnAndFriendsInfo *ownAndFriendsInfo, NotifyHandler *notifyHandler, Controller *mainController) : m_cUdpPacketCreatorAndParser(NULL),
m_cDeviceInterface(deviceInterface),
m_cUdpMainSock(udpMainSock),
m_cOwnAndFriendsInfo(ownAndFriendsInfo),
m_cNotifyHandler(notifyHandler),
m_cController(mainController)
{
	m_cUdpPacketCreatorAndParser = new UdpPacketCreatorAndParser();
}


UdpMessages::~UdpMessages()
{
	if (m_cUdpPacketCreatorAndParser != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cUdpPacketCreatorAndParser.");
#endif	
		delete m_cUdpPacketCreatorAndParser;
		m_cUdpPacketCreatorAndParser = NULL;
	}
}


std::string UdpMessages::GetHotSpotAddr(std::string broadCastAddr)
{
	unsigned int  broadCastArr[4];

	sscanf(broadCastAddr.c_str(), "%d.%d.%d.%d", &broadCastArr[0], &broadCastArr[1], &broadCastArr[2], &broadCastArr[3]);
	broadCastArr[3] = 1;

	return Convert::to_string(broadCastArr[0]) + "." + Convert::to_string(broadCastArr[1]) + "." + Convert::to_string(broadCastArr[2]) + "." + Convert::to_string(broadCastArr[3]);
}


void UdpMessages::SendBroadCastRequest()
{
#ifdef _PRINT_LOG
	printLog("Broadcast::SendBroadCastPacket()");
#endif	

	int numOfInterfaces = m_cDeviceInterface->m_vInterfaceIP4AddressList.size();

	m_stOwnInfo.Reset();
	m_stFriendInfo.Reset();
	m_stOwnInfo = m_cOwnAndFriendsInfo->GetOwnInfo();

	for (int i = 0; i < numOfInterfaces; i++){
		m_stInterfaceInfo = &m_cDeviceInterface->m_vInterfaceIP4AddressList[i];
		m_stOwnInfo.m_nIp = m_stInterfaceInfo->m_nHostAddr;

		(i == 0) ? m_bUsePrevPackId = false : m_bUsePrevPackId = true;
		m_stCreatePacketAttr.SetPacketAttribute(m_stOwnInfo, m_stFriendInfo, m_bUsePrevPackId);
		m_nPacketSize = m_cUdpPacketCreatorAndParser->CreatePacket(UPT_BROADCAST_REQUEST, m_stCreatePacketAttr, m_stDataBuffer.data);
		//	std::string hotSpotAddr = GetHotSpotAddr(broadcastAddr);

		for (int index = BROADCAST_START_PORT; index < BROADCAST_LAST_PORT; index++) {
			m_nSentMsgSize = m_cUdpMainSock->SendTo(m_stDataBuffer.data, m_nPacketSize, m_stInterfaceInfo->m_nBroadcastAddr, index);
		}
	}
}


void UdpMessages::HandleBroadcastRequestMessage()
{
#ifdef _PRINT_LOG
	printLog("UdpMessages::HandleBroadcastRequestMessage()");
#endif	

	int numOfInterfaces = m_cDeviceInterface->m_vInterfaceIP4AddressList.size();

	m_stOwnInfo.Reset();
	m_stFriendInfo.Reset();
	m_stOwnInfo = m_cOwnAndFriendsInfo->GetOwnInfo();
	m_stFriendInfo = m_stParsedPacketAttr.m_stOwnInfo;

	for (int i = 0; i < numOfInterfaces; i++){
		m_stInterfaceInfo = &m_cDeviceInterface->m_vInterfaceIP4AddressList[i];
		m_stOwnInfo.m_nIp = m_stInterfaceInfo->m_nHostAddr;

		i == 0 ? m_bUsePrevPackId = false : m_bUsePrevPackId = true;
		m_stCreatePacketAttr.SetPacketAttribute(m_stOwnInfo, m_stFriendInfo, m_bUsePrevPackId);
		m_stDataBuffer.dataLen = m_cUdpPacketCreatorAndParser->CreatePacket(UPT_BROADCAST_PEER_RESPONSE, m_stCreatePacketAttr, m_stDataBuffer.data);

		m_nSentMsgSize = m_cUdpMainSock->SendTo(m_stDataBuffer.data, m_stDataBuffer.dataLen, m_stFriendInfo.m_nIp, m_stFriendInfo.m_nUdpPort);
	}
}


void UdpMessages::HandleBroadcastPeerResponse()
{
#ifdef _PRINT_LOG
	printLog("UdpMessages::HandleBroadcastPeerResponse()");
#endif	

	m_cOwnAndFriendsInfo->SetOwnIp(m_stParsedPacketAttr.m_stFriendInfo.m_nIp);
	m_stOwnInfo = m_cOwnAndFriendsInfo->GetOwnInfo();

	m_stFriendInfo.Reset();
	m_stFriendInfo = m_stParsedPacketAttr.m_stOwnInfo;

	m_cOwnAndFriendsInfo->UpdateFriendsList(m_stFriendInfo);
#ifdef _PRINT_LOG
	printLog("FrindId: FriendIp : FriendUdpPort : FriendTcpPort : " + Convert::LongToString(m_stFriendInfo.m_nEntityId) + " : " + Convert::LongToString(m_stFriendInfo.m_nIp) + " : " + Convert::to_string(m_stFriendInfo.m_nUdpPort) + " : " + Convert::to_string(m_stFriendInfo.m_nTcpListenPort));
#endif	

	m_stCreatePacketAttr.SetPacketAttribute(m_stOwnInfo, m_stFriendInfo, false);
	m_stDataBuffer.dataLen = m_cUdpPacketCreatorAndParser->CreatePacket(UPT_BROADCAST_CLIENT_RESPONSE, m_stCreatePacketAttr, m_stDataBuffer.data);
	m_nSentMsgSize = m_cUdpMainSock->SendTo(m_stDataBuffer.data, m_stDataBuffer.dataLen, m_stFriendInfo.m_nIp, m_stFriendInfo.m_nUdpPort);
	
	m_cNotifyHandler->NotifyFriendPresents(m_stFriendInfo.m_nEntityId);
}


void UdpMessages::HandleBroadcastClientResponse()
{
#ifdef _PRINT_LOG
	printLog("UdpMessages::HandleBroadcastClientResponse()");
#endif	

	m_cOwnAndFriendsInfo->SetOwnIp(m_stParsedPacketAttr.m_stFriendInfo.m_nIp);
	m_stOwnInfo = m_cOwnAndFriendsInfo->GetOwnInfo();

	m_stFriendInfo.Reset();
	m_stFriendInfo = m_stParsedPacketAttr.m_stOwnInfo;

	m_cOwnAndFriendsInfo->UpdateFriendsList(m_stFriendInfo);
#ifdef _PRINT_LOG
	printLog("FrindId: FriendIp : FriendUdpPort : FriendTcpPort : " + Convert::LongToString(m_stFriendInfo.m_nEntityId) + " : " + Convert::LongToString(m_stFriendInfo.m_nIp) + " : " + Convert::to_string(m_stFriendInfo.m_nUdpPort) + " : " + Convert::to_string(m_stFriendInfo.m_nTcpListenPort));
#endif	

	m_cNotifyHandler->NotifyFriendPresents(m_stFriendInfo.m_nEntityId);
}


void UdpMessages::ProcessReceivedData(struct DataBuffer  &receivedDataBuffer)
{
#ifdef _PRINT_LOG
//	printLog("UdpMessages::ProcessReceivedData()");
#endif	

	if (receivedDataBuffer.dataLen == 0 || receivedDataBuffer.dataLen == -1){
		return;
	}

	m_cUdpPacketCreatorAndParser->ParsePacket(receivedDataBuffer.data, receivedDataBuffer.dataLen, m_eUdpPacketType, m_stParsedPacketAttr, m_stDataBuffer);
			
	switch (m_eUdpPacketType) 
	{
	case UPT_BROADCAST_REQUEST:
		if (m_stParsedPacketAttr.m_stOwnInfo.m_nEntityId == m_cOwnAndFriendsInfo->GetOwnId()){
			return;
		}
		if (memcmp(m_stParsedPacketAttr.m_sPacketId, m_stPackId.m_sBroadcastRequestId, PACKET_ID_SIZE) != 0){
			memcpy(m_stPackId.m_sBroadcastRequestId, m_stParsedPacketAttr.m_sPacketId, PACKET_ID_SIZE);
			HandleBroadcastRequestMessage();
		}
		break;

	case UPT_BROADCAST_PEER_RESPONSE:
		if (memcmp(m_stParsedPacketAttr.m_sPacketId, m_stPackId.m_sBroadcastPeerResponseId, PACKET_ID_SIZE) != 0){
			memcpy(m_stPackId.m_sBroadcastPeerResponseId, m_stParsedPacketAttr.m_sPacketId, PACKET_ID_SIZE);
			HandleBroadcastPeerResponse();
		}
		break;
	
	case UPT_BROADCAST_CLIENT_RESPONSE:
		if (memcmp(m_stParsedPacketAttr.m_sPacketId, m_stPackId.m_sBroadcastClientResponseId, PACKET_ID_SIZE) != 0){
			memcpy(m_stPackId.m_sBroadcastClientResponseId, m_stParsedPacketAttr.m_sPacketId, PACKET_ID_SIZE);
			HandleBroadcastClientResponse();
		}
		break;

	case UPT_CHAT_MSG:
		if (memcmp(m_stParsedPacketAttr.m_sPacketId, m_stPackId.m_sChatMessage, PACKET_ID_SIZE) != 0){
			memcpy(m_stPackId.m_sChatMessage, m_stParsedPacketAttr.m_sPacketId, PACKET_ID_SIZE);
			HandleChatMessage();
		}
		break;

	case UPT_CHAT_MSG_CONFIRM:
		if (memcmp(m_stParsedPacketAttr.m_sPacketId, m_stPackId.m_sChatMessageConfirm, PACKET_ID_SIZE) != 0){
			memcpy(m_stPackId.m_sChatMessageConfirm, m_stParsedPacketAttr.m_sPacketId, PACKET_ID_SIZE);
			HandleChatConfirmMessage();
		}
		break;

	default:
#ifdef _PRINT_LOG
		printLog("Message Not Matched with Any case!!!");
#endif	
		break;
	}	
}


void UdpMessages::HandleChatMessage()
{
//	std::cout << "UdpMessages::HandleChatMessage()" << std::endl;

	m_stDataBuffer.data[m_stDataBuffer.dataLen] = '\0';
	std::string chatMessage = std::string((char *)m_stDataBuffer.data);

	m_stCurFriendInfo.Reset();
	m_stCurFriendInfo = m_stParsedPacketAttr.m_stOwnInfo;

	m_stCreatePacketAttr.SetPacketAttribute(m_stOwnInfo, m_stCurFriendInfo, false);
	m_stDataBuffer.dataLen = m_cUdpPacketCreatorAndParser->CreatePacket(UPT_CHAT_MSG_CONFIRM, m_stCreatePacketAttr, m_stDataBuffer.data);

	for (int i = 0; i < 15; i++){
		m_nSentMsgSize = m_cUdpMainSock->SendTo(m_stDataBuffer.data, m_stDataBuffer.dataLen, m_stCurFriendInfo.m_nIp, m_stCurFriendInfo.m_nUdpPort);
		Convert::SOSleep(UDP_PACK_RESEND_INTERVAL_IN_ML_SECOND);
	}

	m_cNotifyHandler->NotifyChatMessages(m_stCurFriendInfo.m_nEntityId, chatMessage);
}


void UdpMessages::HandleChatConfirmMessage()
{
	m_bIsChatMessageSent = true;
}


bool UdpMessages::SendChatMessage(IPVLongType friendId, std::string message)
{
	m_bIsChatMessageSent = false;

	if (m_cOwnAndFriendsInfo->IsFriendExist(friendId)){
		m_stCurFriendInfo.Reset();
		m_stCurFriendInfo = m_cOwnAndFriendsInfo->GetFriendInfo(friendId);

		m_stCreatePacketAttr.SetPacketAttribute(m_stOwnInfo, m_stCurFriendInfo, false, true);
		m_stDataBuffer.dataLen = m_cUdpPacketCreatorAndParser->CreatePacket(UPT_CHAT_MSG, m_stCreatePacketAttr, m_stDataBuffer.data, (char *)message.c_str(), message.size());

		int i = 0;
		while (m_bIsChatMessageSent == false){
			m_nSentMsgSize = m_cUdpMainSock->SendTo(m_stDataBuffer.data, m_stDataBuffer.dataLen, m_stCurFriendInfo.m_nIp, m_stCurFriendInfo.m_nUdpPort);
			Convert::SOSleep(UDP_PACK_RESEND_INTERVAL_IN_ML_SECOND);
			i++;
			if (i == (CHAT_MESSAGE_SEND_TRYING_TIME / UDP_PACK_RESEND_INTERVAL_IN_ML_SECOND)){
				return false;
			}
		}
	}
	else{
		return false;
	}
	
	return true;
}