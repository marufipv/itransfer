//
//  ObjectivecInterface.h
//  ITransferSdkCocoa
//
//  Created by zulfiker shishir on 12/28/15.
//  Copyright © 2015 zulfiker shishir. All rights reserved.
//

#ifndef ObjectivecInterface_h
#define ObjectivecInterface_h

#ifdef WIN32
typedef __int64 IPVLongType;
#else
typedef long long IPVLongType;
#endif

//#ifdef __cplusplus
//extern "C" {
//#endif

void ConnectForMultipeerConnectivity(void *myObjectInstance, IPVLongType userName);
void SendInvitionForMultipeer(void *myObjectInstance, IPVLongType frindId);
void* SendFileforMc(void *myObjectInstance,std::string filePath, std::string fileName,IPVLongType friendId);


void NotifyiTransferFriendPresentsIos(IPVLongType friendID);
void NotifyFileTransferRateIos(int type, std::string fileName, int receivedRatio);


//#ifdef __cplusplus
//}
//#endif

#endif /* ObjectivecInterface_h */
