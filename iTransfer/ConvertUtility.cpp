#include "ConvertUtility.h"

namespace  Convert
{
	std::string to_string(unsigned int value)
	{
		std::ostringstream os;
		os << value;
		return os.str();
	}

	std::string LongToString(long long value)
	{
		std::ostringstream os;
		os << value;
		return os.str();
	}

	std::string getInterfaceName(int type)
	{
		std::string interfaceName;

		if (type == 1){
			interfaceName = "INTERFACE_TYPE_ETHERNET";
		}
		else if (type == 2){
			interfaceName = "INTERFACE_TYPE_WIFI";
		}
		else if (type == 3){
			interfaceName = "INTERFACE_TYPE_TUNNEL";
		}
		else if (type == 4){
			interfaceName = "INTERFACE_TYPE_3G";
		}
		else if (type == 5){
			interfaceName = "INTERFACE_TYPE_OTHER";
		}

		return interfaceName;
	}


	void ShortToByteArray(unsigned short num, unsigned char *sByteArr)
	{
		sByteArr[0] = (num >> 8) & 0xFF;
		sByteArr[1] = (num & 0xFF);
	}


	unsigned short ByteArrayToShort(unsigned char *sByteArr)
	{
		unsigned short num;

		num = num & 0x0000;
		num = (num | sByteArr[0]) << 8;
		num = (num | sByteArr[1]);

		return num;
	}


	void IntToByteArray(int num, unsigned char *sByteArr)
	{
		sByteArr[0] = (num >> 24) & 0xFF;
		sByteArr[1] = (num >> 16) & 0xFF;
		sByteArr[2] = (num >> 8) & 0xFF;
		sByteArr[3] = (num & 0xFF);
	}


	int ByteArrayToInt(unsigned char *sByteArr)
	{
		int num;

		num = num & 0x00000000;
		num = (num | sByteArr[0]) << 8;
		num = (num | sByteArr[1]) << 8;
		num = (num | sByteArr[2]) << 8;
		num = (num | sByteArr[3]);

		return num;
	}


	void LongToByteArray(unsigned long long num, unsigned char *sByteArr)
	{
		sByteArr[0] = (num >> 56) & 0xFF;
		sByteArr[1] = (num >> 48) & 0xFF;
		sByteArr[2] = (num >> 40) & 0xFF;
		sByteArr[3] = (num >> 32) & 0xFF;
		sByteArr[4] = (num >> 24) & 0xFF;
		sByteArr[5] = (num >> 16) & 0xFF;
		sByteArr[6] = (num >> 8) & 0xFF;
		sByteArr[7] = (num & 0xFF);
	}


	unsigned long long ByteArrayToLong(unsigned char *sByteArr)
	{
		unsigned long long num;

		num = num & 0x0000000000000000;
		num = (num | sByteArr[0]) << 8;
		num = (num | sByteArr[1]) << 8;
		num = (num | sByteArr[2]) << 8;
		num = (num | sByteArr[3]) << 8;
		num = (num | sByteArr[4]) << 8;
		num = (num | sByteArr[5]) << 8;
		num = (num | sByteArr[6]) << 8;
		num = (num | sByteArr[7]);

		return num;
	}

    std::string Interger32IPAddresstoString(unsigned int  &addr)
    {
        unsigned char bytes[4];
        char ch[20];
        bytes[0] = addr & 0xFF;
        bytes[1] = (addr >> 8) & 0xFF;
        bytes[2] = (addr >> 16) & 0xFF;
        bytes[3] = (addr >> 24) & 0xFF;
        sprintf(ch, "%d.%d.%d.%d", bytes[0], bytes[1], bytes[2], bytes[3]);
        std::string str = std::string(ch);
        
        return str;
    }
    
    void CharIPAddressToInterger32(char *addr, unsigned int &iaddr)
    {
    
       iaddr = inet_addr(addr);
    }

	void SOSleep(int Timeout)
	{
#ifdef WIN32 
		Sleep(Timeout);
#else
		timespec t;
		u_int32_t seconds = Timeout / 1000;
		t.tv_sec = seconds;
		t.tv_nsec = (Timeout - (seconds * 1000)) * (1000 * 1000);
		nanosleep(&t, NULL);
#endif
	}
}
