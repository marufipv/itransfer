#ifndef _I_TRANSFER_C_INTERFACE_H
#define _I_TRANSFER_C_INTERFACE_H


#include "iTransfer.h"

extern "C" {
	void iTransfer_init(IPVLongType userName, std::string fileReceivePath);

	void iTransfer_FindFriend();
	bool iTransfer_SendChatMsg(IPVLongType friendId, std::string message);
	void iTransfer_SendFile(IPVLongType FriendName, std::vector<std::string> sendFileList);

	void iTransfer_CloseCurrentSession();
	void iTransfer_CloseiTransfer();
}



#endif