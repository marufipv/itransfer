#ifndef _COMMON_DATA_H
#define _COMMON_DATA_H

#ifdef USE_JNI
#include "JNILinker.h"
#include <android/log.h>
#endif

#ifdef WIN32
typedef __int64 IPVLongType;
#else 
typedef long long IPVLongType;
#endif

#define BROADCAST_START_PORT   5100
#define BROADCAST_LAST_PORT    5111

#define MAXIMUM_BUFFER_SIZE    4096
#define PACKET_ID_SIZE         24

#define CHAT_MESSAGE_SEND_TRYING_TIME          1750
#define UDP_PACK_RESEND_INTERVAL_IN_ML_SECOND  25

#ifdef  WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>

#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>

#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif

#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif

#endif

enum  InterfaceType{
	INTERFACE_TYPE_ETHERNET = 1,
	INTERFACE_TYPE_WIFI = 2,
	INTERFACE_TYPE_TUNNEL = 3,
	INTERFACE_TYPE_3G = 4,
	INTERFACE_TYPE_OTHER = 5
};


enum FileTransferNotifyType{
	NOTIFY_TYPE_RATIO = 1,
	NOTIFY_TYPE_RATE = 2
};


enum TcpPacketType
{
	TPT_FILE_SEND_INIT,
	TPT_FILE_INFO_SIZE,

	TPT_FILE_SEND_START,
	TPT_FILE_SEND_FINISH,
	TPT_FILE_SEND_FINISH_RESPONSE,
};

enum UdpPacketType
{
	UPT_BROADCAST_REQUEST = 1,
	UPT_BROADCAST_PEER_RESPONSE = 2,
	UPT_BROADCAST_CLIENT_RESPONSE = 3,

	UPT_CHAT_MSG = 4,
	UPT_CHAT_MSG_CONFIRM = 5
};


enum UdpPacketAttributeType
{
	UPAT_PACKET_ID,

	UPAT_CLIENT_ID,
	UPAT_CLIENT_IP,
	UPAT_CLIENT_UDP_PORT,
	UPAT_CLIENT_TCP_LISTEN_PORT,

	UPAT_FRIEND_ID,
	UPAT_FRIEND_IP,
	UPAT_FRIEND_UDP_PORT,
	UPAT_FRIEND_TCP_LISTEN_PORT,

	UPAT_RAW_DATA
};


#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <thread>

#include "StructEntityInfo.h"
#include "NotifyHandler.h"
#include "StructInterfaceInfo.h"
#include "ConvertUtility.h"
#include "StructUdpPacketAttr.h"
#include "StructDataBuffer.h"
#include "DeviceInterface.h"
#include "OwnAndFriendsInfo.h"
#include "Socket.h"
#include "Controller.h"
#include "iTransfer.h"
#include "TcpSocketController.h"
#include "UdpMessages.h"
#include "UdpPacketCreatorAndParser.h"
#include "SendFile.h"
#include "ReceiveFile.h"




inline void printLog(std::string msg)
{
#ifdef USE_JNI
	__android_log_print(ANDROID_LOG_VERBOSE, "iTransfer", "%s", msg.c_str());
#else
	std::cout << msg << std::endl;
#endif

}



#endif