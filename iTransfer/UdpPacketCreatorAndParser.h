#ifndef _UDP_PACKET_CREATOR_AND_PARSER_H
#define _UDP_PACKET_CREATOR_AND_PARSER_H


#include "CommonData.h"


enum UdpPacketType;
enum UdpPacketAttributeType;

class UdpPacketCreatorAndParser
{
private:
	int m_nPacketLen;
	int m_nAttrLen;

public:
	int CreatePacket(const enum UdpPacketType &packetType, const struct PacketAttribute  &packetAttribute, unsigned char *buffer, char *rawData = "", int rawDataSize = -1);
	void ParsePacket(unsigned char *buffer, int packetLen, enum UdpPacketType &packetType, struct PacketAttribute &packetAttribute, struct DataBuffer &dataBuffer);
};




#endif