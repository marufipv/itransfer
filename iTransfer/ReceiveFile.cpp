
#include "ReceiveFile.h"
#include <iostream>

#ifdef USE_JNI
#include "JNILinker.h"
#include <android/log.h>
#endif


CReceiveFile::CReceiveFile(NotifyHandler *notifyHandler, std::string &fileReceivePath) :
m_cReceiver(NULL), 
m_cNotifyHandler(notifyHandler),
m_fpWriteFile(NULL)
{
	m_sFileReceivePath = fileReceivePath;
}


void CReceiveFile::SetReceiver(Socket  *receiver)
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::SetReceiver()");
#endif

	m_cReceiver = receiver;
}


void CReceiveFile::RemoveReceiver()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::RemoveReceiver()");
#endif

	m_cReceiver = NULL;
}


void CReceiveFile::ReceiveFileInitInfo()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::ReceiveFileInitInfo()");
#endif

	int recvedSize, fileInitInfoSize;

	fileInitInfoSize = 5;
	recvedSize = m_cReceiver->Recv((char *)m_sBuffer, fileInitInfoSize);

	if (int(m_sBuffer[0]) == TPT_FILE_SEND_INIT){
		m_nNumOfFile = Convert::ByteArrayToInt(&m_sBuffer[1]);
	}
	else{
		m_nNumOfFile = -1;
	}
}


void CReceiveFile::OpenFile()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::OpenFile()");
#endif	

	char filePath[500];

	strcpy(filePath, (char *)m_sFileReceivePath.c_str());
	strcat(filePath, m_sFileName);
	m_fpWriteFile = fopen(filePath, "wb");
}


int CReceiveFile::ReceiveFileInfoLenMsg()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::ReceiveFileInfoLenMsg()");
#endif

	int recvedSize, infoSize;

	if (m_bIsRunningFileReceivingThread == false){
		return -1;
	}

	infoSize = -1;
	recvedSize = m_cReceiver->Recv((char *)m_sBuffer, 5);
	if (recvedSize == -2){
		m_bIsRunningFileReceivingThread = false;
		return -1;
	}

	if (int(m_sBuffer[0]) == TPT_FILE_INFO_SIZE){
		infoSize = Convert::ByteArrayToInt(&m_sBuffer[1]);
	}

#ifdef _PRINT_LOG
	printLog("InfoSize: " + Convert::to_string(infoSize));
#endif

	return infoSize;
}


bool CReceiveFile::ReceiveFileInfo()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::ReceiveFileInfo()");
#endif

	int  pos, fileNameLen, recvedSize, infoSize;

	if (m_bIsRunningFileReceivingThread == false){
		return false;
	}

	infoSize = ReceiveFileInfoLenMsg();

	if (infoSize != -1){
		recvedSize = m_cReceiver->Recv((char *)m_sBuffer, infoSize);
		if (recvedSize == -2){
			m_bIsRunningFileReceivingThread = false;
			return false;
		}

		if (int(m_sBuffer[0]) == TPT_FILE_SEND_START){
			pos = 1;
			m_nFileDataSize = Convert::ByteArrayToLong(&m_sBuffer[pos]);          pos = pos + 8;
			fileNameLen = Convert::ByteArrayToInt(&m_sBuffer[pos]);               pos = pos + 4;
			memcpy(m_sFileName, (char *)&m_sBuffer[pos], fileNameLen);            pos = pos + fileNameLen;
			m_sFileName[fileNameLen] = '\0';

#ifdef _PRINT_LOG
			printLog("FileName: " + std::string(m_sFileName) + "  FileSize: " + Convert::LongToString(m_nFileDataSize));
//			std::cout << "RecvedSize: " << recvedSize << "  Pos: " << pos << std::endl;
#endif

			m_cNotifyHandler->NotifyFileTransferInfo(NOTIFY_TYPE_RATIO, m_sFileName, 0);
			m_tStartTime = time(NULL);

			OpenFile();
			m_nRemainingDataSize = m_nFileDataSize;

			return true;
		}
	}
	else{
		return false;
	}
    
    return false;
}


bool CReceiveFile::ReceiveFileData()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::ReceiveFileData()");
#endif

	int recvedSize, recvedRatio, lastRatio;

	if (m_bIsRunningFileReceivingThread == false){
		return false;
	}

	if ((m_fpWriteFile == NULL) || (m_cReceiver == NULL)){
		return false;
	}

	lastRatio = 0;
	while (m_nRemainingDataSize > 0){
		if (m_nRemainingDataSize >= RECEIVE_BUFFER_SIZE){
			recvedSize = m_cReceiver->Recv((char *)m_sBuffer, RECEIVE_BUFFER_SIZE);
		}
		else{
			recvedSize = m_cReceiver->Recv((char *)m_sBuffer, m_nRemainingDataSize);
		}

		if (recvedSize == -2){
			m_bIsRunningFileReceivingThread = false;
			return false;
		}

		fwrite(m_sBuffer, 1, recvedSize, m_fpWriteFile);
		m_nRemainingDataSize = m_nRemainingDataSize - recvedSize;

		recvedRatio = (((m_nFileDataSize - m_nRemainingDataSize) * 100) / m_nFileDataSize);
		if (((lastRatio + 5) <= recvedRatio) && (recvedRatio != 100)){
			m_cNotifyHandler->NotifyFileTransferInfo(NOTIFY_TYPE_RATIO, m_sFileName, recvedRatio);
			lastRatio = recvedRatio;
            
		}

#ifdef _PRINT_LOG
//		printLog("ReceivedSize: " + Convert::to_string(recvedSize) + "TotRecvedSize : " + Convert::to_string(m_nFileDataSize - m_nRemainingDataSize) );
#endif
	}
    
    return false;
}


void CReceiveFile::ReceiveFileFinishSignal()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::ReceiveFileFinishSignal()");
#endif

	int recvedSize, rate;

	if (m_bIsRunningFileReceivingThread == false){
		return;
	}

	if ((m_fpWriteFile == NULL) || (m_cReceiver == NULL)){
		return;
	}

	recvedSize = m_cReceiver->Recv((char *)m_sBuffer, 5);

	if (recvedSize == -2){
		m_bIsRunningFileReceivingThread = false;
		return;
	}

	if (int(m_sBuffer[0]) == TPT_FILE_SEND_FINISH){
		CloseFile();
	}

//	SendFileFinishResponseSignal();
	m_cNotifyHandler->NotifyFileTransferInfo(NOTIFY_TYPE_RATIO, m_sFileName, 100);

	m_tEndTime = time(NULL);
	m_tDiffTime = (m_tEndTime - m_tStartTime);

#ifdef _PRINT_LOG
	printLog("======== Total Elapsed Time: " + Convert::to_string(m_tDiffTime));
	printLog("======== Total File Size: " + Convert::LongToString(m_nFileDataSize));
#endif

	if (m_tDiffTime){
		rate = ((m_nFileDataSize / m_tDiffTime) / (128 * 1024));
#ifdef _PRINT_LOG
		printLog("======== Data Rate: " + Convert::to_string(rate) + " (Mb/Second)");
#endif
	}
	else{
		rate = (m_nFileDataSize / (128 * 1024));
		if (rate){
#ifdef _PRINT_LOG
			printLog("======== Data Rate: " + Convert::to_string(rate) + " (Mb/Second)");
#endif
		}
		else{
			rate = 10;
#ifdef _PRINT_LOG
			printLog("======== Data Rate: " + Convert::to_string(rate) + " (Mb/Second)");
#endif
		}
	}
	m_cNotifyHandler->NotifyFileTransferInfo(NOTIFY_TYPE_RATE, m_sFileName, rate);

}


void CReceiveFile::SendFileFinishResponseSignal()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::SendFileFinishResponseSignal()");
#endif

	m_sBuffer[0] = char(TPT_FILE_SEND_FINISH_RESPONSE);
	m_cReceiver->Send((char*)m_sBuffer, 1);
}


void CReceiveFile::CloseFile()
{
#ifdef _PRINT_LOG
	printLog("ReceiveFile::CloseFile()");
#endif

	if (m_fpWriteFile != NULL){
		fclose(m_fpWriteFile);
		m_fpWriteFile = NULL;
	}
}


void CReceiveFile::ReceiveFile()
{
#ifdef _PRINT_LOG
	printLog("CReceiveFile::ReceiveFile()");
#endif

	int i = 0;

	m_bIsRunningFileReceivingThread = true;
	while (m_bIsRunningFileReceivingThread){
		if (ReceiveFileInfo()){
			ReceiveFileData();
			ReceiveFileFinishSignal();
		}
	}

//	CloseFile();


//	ReceiveFileInitInfo();

//	std::cout << "NumOfRecvingFile: " << m_nNumOfFile << std::endl;
//	while (i < m_nNumOfFile){
//	}
}
