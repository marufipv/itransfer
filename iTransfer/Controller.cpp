
#include "Controller.h"


using namespace std;

Controller::Controller(IPVLongType userName, std::string fileReceivePath) : m_nUserName(userName), m_sFileReceivingPath(fileReceivePath),
m_cUdpMessage(NULL), m_cDeviceInterface(NULL), m_cUdpMainSock(NULL)
{
	m_cNotifyHandler = new NotifyHandler();
	m_cOwnAndFriendsInfo = new OwnAndFriendsInfo(userName);
	m_cTcpSocketsController = new TcpSocketsController(this, m_cNotifyHandler, m_cOwnAndFriendsInfo, fileReceivePath);

	GatherHostInterface();
	CreateUDPGeneralSocket();
}


Controller::~Controller()
{
	StopUDPDataReceiveThread();

	if (m_cNotifyHandler != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cNotifyHandler");
#endif
		delete m_cNotifyHandler;
		m_cNotifyHandler = NULL;
	}

	if (m_cOwnAndFriendsInfo != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cOwnAndFriendsInfo");
#endif
		delete m_cOwnAndFriendsInfo;
		m_cOwnAndFriendsInfo = NULL;
	}

	if (m_cTcpSocketsController != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cTcpSocketsController");
#endif
		delete m_cTcpSocketsController;
		m_cTcpSocketsController = NULL;
	}

	if (m_cDeviceInterface != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cTcpSocketsController");
#endif
		delete m_cDeviceInterface;
		m_cDeviceInterface = NULL;
	}

	if (m_cUdpMainSock != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cUdpMainSock");
#endif
		delete m_cUdpMainSock;
		m_cUdpMainSock = NULL;
	}

	if (m_cUdpMessage != NULL){
#ifdef _PRINT_LOG
		printLog("Delete m_cUdpMessage");
#endif
		delete m_cUdpMessage;
		m_cUdpMessage = NULL;
	}

#ifdef _PRINT_LOG
	printLog("~Controller");
#endif
}



void Controller::GatherHostInterface()
{
	m_cDeviceInterface = new DeviceInterface();
	m_cDeviceInterface->GatherHostInterfaceInfo();
}


void Controller::CreateUDPGeneralSocket()
{
	m_nOwnBindIp = INADDR_ANY;

	m_cUdpMainSock = new Socket(true);
	m_cUdpMainSock->CreateSocket();
	m_cUdpMainSock->EnableSocketBroadcastOption();
	m_cUdpMainSock->SetRecvTimeOut();

	for (int i = BROADCAST_START_PORT; i <= BROADCAST_LAST_PORT; i++){
		if (m_cUdpMainSock->BindSocket(m_nOwnBindIp, i)){
			break;
		}
	}

	m_cOwnAndFriendsInfo->SetOwnUdpPort(m_cUdpMainSock->GetBindPort());
	m_cUdpMessage = new UdpMessages(m_cDeviceInterface, m_cUdpMainSock, m_cOwnAndFriendsInfo, m_cNotifyHandler, this);

	std::thread udpDataRecvThread = StartUDPDataReceiveThread();
	udpDataRecvThread.detach();
	
	FindNearbyFriend();
}


void Controller::HandleUDPDataReceiveThread()
{
#ifdef _PRINT_LOG
	printLog("Controller::HandleUDPDataReceiveThread()");
#endif

	m_bIsRunningUDPDataReceivingThread = true;
	m_bIsClosedUDPDataReceivingThread = false;

	while (m_bIsRunningUDPDataReceivingThread)
	{
		m_stDataBuffer.dataLen = m_cUdpMainSock->RecvFrom(m_stDataBuffer.data, MAXIMUM_BUFFER_SIZE, m_stDataBuffer.clientIp, m_stDataBuffer.clientPort);
		if (m_stDataBuffer.dataLen != -1){
			m_cUdpMessage->ProcessReceivedData(m_stDataBuffer);
		}
	}

	m_bIsClosedUDPDataReceivingThread = true;
}


std::thread Controller::StartUDPDataReceiveThread()
{
	return std::thread([=] { HandleUDPDataReceiveThread(); });
}


void Controller::StopUDPDataReceiveThread()
{
#ifdef _PRINT_LOG
	printLog("Controller::StopUDPDataReceiveThread()");
#endif

	if (m_cUdpMainSock != NULL){
		m_cUdpMainSock->CloseSocket();
	}

	m_bIsRunningUDPDataReceivingThread = false;
	while (!m_bIsClosedUDPDataReceivingThread){
		Convert::SOSleep(5);
	}
}


void Controller::FindNearbyFriend()
{
	if (m_cUdpMessage != NULL){
		m_cUdpMessage->SendBroadCastRequest();
	}
}

bool Controller::SendChatMessage(IPVLongType friendId, std::string message)
{
	if (m_cUdpMessage != NULL){
		return m_cUdpMessage->SendChatMessage(friendId, message);
	}

	return false;
}


void Controller::SendFile(IPVLongType FriendName, std::vector<std::string> filePathList)
{
	if (m_cTcpSocketsController != NULL){
		m_cTcpSocketsController->SendFile(FriendName, filePathList);
	}
}

void Controller::CloseCurrentSession()
{
	if (m_cTcpSocketsController != NULL){
		m_cTcpSocketsController->CloseCurrentSession();
	}
}


void Controller::SetNotifyFriendPresents(void(*ptr)(IPVLongType))
{
	if (m_cNotifyHandler != NULL){
		m_cNotifyHandler->SetNotifyFriendPresents(ptr);
	}
}


void Controller::SetNotifyChatMessages(void(*ptr)(IPVLongType, std::string))
{
	if (m_cNotifyHandler != NULL){
		m_cNotifyHandler->SetNotifyChatMessages(ptr);
	}
}


void Controller::SetNotifyFileTransferInfo(void(*ptr)(int, std::string, int))
{
	if (m_cNotifyHandler != NULL){
		m_cNotifyHandler->SetNotifyFileTransferInfo(ptr);
	}
}