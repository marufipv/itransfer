#ifndef _STRUCT_INTERFACE_INFO_H
#define _STRUCT_INTERFACE_INFO_H

#include "CommonData.h"


struct InterfaceInfo
{
	unsigned int m_nHostAddr;
	std::string m_sHostAddr;
	int m_nInterfaceType;
	unsigned int m_nBroadcastAddr;
	std::string m_sBroadcastAddr;

	InterfaceInfo(unsigned int nHostAddr, std::string sHostAddr, int interfaceType, unsigned int nBroadcastAddr, std::string sBroadcastAddr){
		m_nHostAddr = nHostAddr;
		m_sHostAddr = sHostAddr;
		m_nInterfaceType = interfaceType;
		m_nBroadcastAddr = nBroadcastAddr;
		m_sBroadcastAddr = sBroadcastAddr;
	}
};

#endif