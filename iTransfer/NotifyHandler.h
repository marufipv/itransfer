#ifndef _NOTIFY_HANDLER_H
#define _NOTIFY_HANDLER_H


#include "CommonData.h"


class NotifyHandler
{
private:
	void(*NotifyFriendPresentsCallBack)(IPVLongType);
	void(*NotifyChatMessagesCallBack)(IPVLongType, std::string);
	void(*NotifyFileTransferInfoCallBack)(int, std::string, int);

public:
	NotifyHandler();

	void SetNotifyFriendPresents(void(*ptr)(IPVLongType));
	void NotifyFriendPresents(IPVLongType friendId);

	void SetNotifyChatMessages(void(*ptr)(IPVLongType, std::string));
	void NotifyChatMessages(IPVLongType friendId, std::string &message);

	void SetNotifyFileTransferInfo(void(*ptr)(int, std::string, int));
	void NotifyFileTransferInfo(int type, std::string fileName, int value);
};


#endif